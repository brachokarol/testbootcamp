"use strict";
/**
 * Escribir una funcion que acepte como parametro una cadena de texto
 * y nos devuelva si es palindromo o no
 * Ejemplos palindromos:
 *  "Arriba la birra"
 *  "Luz azul"
 *  "Ana "
 *  "Ana"
 *  "A cara rajada da jararaca"
 *  "Ecco delle belle docce"
 *
 * Implementar con los metodos de los arrays y sin utilizarlos.
 */

function esPalindromo(texto) {
  let palabra1 = texto.toLowerCase().split(" ").join("");
  // let palabra2 = palabra1.split(" ").join("").split("").reverse().join("");
  let palabra2 = [...palabra1].reverse().join("");

  // return (palabra1 === palabra2 ? "Es palindromo" : "No es palindromo");
  return palabra1 === palabra2;
}

console.log(esPalindromo("Arriba la birra"));
console.log(esPalindromo("Karol"));
console.log(esPalindromo("Luz azul"));
console.log(esPalindromo("Ana "));
console.log(esPalindromo("Ana"));
console.log(esPalindromo("A cara rajada da jararaca"));
console.log(esPalindromo("Ecco delle belle docce"));
console.log(esPalindromo("Karola"));
