/* Crear una función que reciba un Array como parámetro
 y retorne otro Array con los contenidos del Array inicial 
 pero eliminando los duplicados.

Ejecuta esa función pasándole como parámetro el Array e 
imprime en la consola el resultado. */

const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

/* con for/of e include() similar a la implementación 
con reduce pero más fácil de entender */
const eliminarDuplicado = (arrayNames) => {
  const newArray = [];
  for (const name of arrayNames) {
    if (!newArray.includes(name)) {
      newArray.push(name);
    }
  }
  return newArray;
}
console.log(eliminarDuplicado(names));


//con Array.reduce() e include()
// function eliminarDuplicado(array) {
//   return array.reduce((acc, name) => {
//     if (!acc.includes(name)) {
//       acc.push(name);
//     }
//     return acc;
//   }, []);
// }
// // console.log(names);
// console.log(eliminarDuplicado(names));


//probando Set()
// function eliminarDuplicado(dataArray) {
//   return [...new Set(dataArray)];
// }
// console.log(eliminarDuplicado(names));