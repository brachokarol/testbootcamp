/* Una pizzería ofrec pizzas de dos sabores.
Crear función que reciba un array con los tipos de pizzas y
retorne otro array con las posibles combinaciones.

Notas:
Evitar las mismas combinaciones, por ejemplo:
margarita-tropical = tropical-margarita.

Evitar repetir el mismo sabor en una combinación, ya que no sería dos sabores.
ej: carbonara-carbonara.
*/

const pizzas = [
  "margarita",
  "cuatro quesos",
  "prosciutto",
  "carbonara",
  "barbacoa",
  "tropical",
];

function combine(pizzas) {
  const combinations = [];
  for (let i = 0; i < pizzas.length - 1; i++) {
    for (let j = i + 1; j < pizzas.length; j++) {
      combinations.push(`${pizzas[i]} y ${pizzas[j]}`);
    }
  }
  return combinations;
}
console.log(combine(pizzas));