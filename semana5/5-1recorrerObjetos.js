// /* Crea una función externa al bucle 
// que en cada iteración reciba dos valores name, age y 
// que se encarge de imprimir en la consola la cadena de texto 
// solicitada para cada persona del objeto people.*/

const people = {
  Maria: 20,
  Ana: 14,
  Luis: 16,
  Pepe: 35,
  Manuel: 50,
  Teresa: 12,
  Daniel: 27,
  Irene: 23,
  Alex: 10,
};
const mayoriaEdad = 18;

function displayData(name, age) {
  console.log(age > 18 ? `${name} es mayor de edad` : `${name} es menor de edad`);
}

//con for/in más compacto
for (const key in people) {
  displayData(key, people[key]);
}

//con for/in paso a paso
// for (const key in people) {
//   const name = key;
//   const age = people[key];
//   displayData(name, age);
// }


//con for tradicional
// let name = Object.keys(people);
// let age = Object.values(people);

// for (let i = 0; i < name.length; i++) {
//   displayData(name[i], age[i]);
// }