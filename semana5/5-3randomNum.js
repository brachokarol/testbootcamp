"use-strict";
/* Crear una función quiz que:
1.Genere un número aleatorio entre 0 y 100 y lo guarde en una variable,
este número será el que el usuario deba adivinar.

2.Muestre una ventana de diálogo tipo prompt que pida al usuario que 
introduzca un número entre 0 y 100

3.Si el número introducido es igual al generado en el punto 1, 
aparecerá en pantalla una ventana de diálogo tipo alert que informará al usuario que ha ganado;
si no, el alert indicará si el número introducido es mayor o menor al que tiene que adivinar y
dará una nueva oportunidad. 

Solo tendrá 5 oportunidades*/

function quiz() {
  const randomNum = Math.floor(Math.random() * 101);
  let lifes = 5;
  let isFinish = false;
  let userGuess;
  alert("Solo tienes 5 intentos")
  do {
    userGuess = Number.parseInt(prompt(`Introduce un num entre 0 y 100 (intentos: ${6 - lifes} / 5)`));
    if (userGuess === randomNum) {
      alert(`Nailed it!! HAS GANADO al ${(lifes == 1) ? "último" : (6 - lifes)} intento!!! `);
      isFinish = true;
    }
    else if (userGuess > randomNum) {
      --lifes;
      alert("El num introducido es mayor");
    }
    else if (userGuess < randomNum) {
      --lifes;
      alert("El num introducido es menor");
    }
    if (lifes === 0) {
      alert(`No tienes más intentos.El num era: ${randomNum} `);
      isFinish = true;
    }
  } while (isFinish == false);
}

quiz();