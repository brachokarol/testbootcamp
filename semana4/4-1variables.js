//SEMANA 4 - EJERCICIOS
// **************ejercicio 1 - Variables básicas************
// const nombre = "Marta";
// const edad = 27;
// const color = "púrpura";
// console.log(`Hola, me llamo ${nombre}, tengo ${edad} años y mi color favorito es el ${color}.`);

//****************ejercicio 2 - Condicionales****************
// const nombre = "Marta";
// const edad = 27;
// let descuento = "";
// let mensaje = `A ${nombre} le corresponde el descuento`;

// if (edad < 12) {
//   descuento = "infantil (menores de 12 años)";
// }
// else if (edad < 30) {
//   descuento = "juvenil (menores de 30 años)";
// }
// else if (edad > 60) {
//   descuento = "de jubilados (mayores de 60 años)";
// }
// else {
//   mensaje = `A ${nombre} no le corresponde`;
//   descuento = "ningún descuento";
// }
// mensaje += " " + descuento;

// console.log(`${mensaje}`);

//*******************ejercicio 3 - Bucles básicos***********
// for (let i = 0; i <= 23; i++) {
//   let mensaje = `Son las ${i}:00`;
//   if (i >= 8 && i <= 22) {
//     mensaje += ", CUCÚ!"";
//   }
//   console.log(mensaje);
// }

//*******************ejercicio 4 - Bucles complejos***********
//No utilizo la función repeat porque me parece no la hemos visto
// for (let i = 0; i <= 23; i++) {
//   let mensaje = `Son las ${i}:00`;
//   let cucu = "CUCÚ! ";
//   if (i >= 8 && i <= 22) {
//     if (i == 8 || i == 20) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 9 || i == 21) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 10 || i == 22) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 11) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 12) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 13) {
//       mensaje += `. ${cucu}`;
//     }
//     else if (i == 14) {
//       mensaje += `. ${cucu}${cucu}`;
//     }
//     else if (i == 15) {
//       mensaje += `. ${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 16) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 17) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 18) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//     else if (i == 19) {
//       mensaje += `. ${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}${cucu}`;
//     }
//   }
//   console.log(mensaje);
// }

//*******************ejercicio 4 - Bucles complejos***********
//Usando Repeat
for (let i = 0; i <= 23; i++) {
  let mensaje = `Son las ${i}:00`;
  let cucu = " CUCÚ";

  if (i >= 8 && i <= 12) {
    mensaje += `. ${cucu.repeat(i)}`;
  }
  if (i > 12 && i <= 22) {
    mensaje += `. ${cucu.repeat(i - 12)}`;
  }
  console.log(mensaje);
}

