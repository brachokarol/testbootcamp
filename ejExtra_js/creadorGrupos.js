"use strict";

const students = [
  "Carlos Pita Sánchez",
  "David Martínez Quiroga",
  "Eduardo Andrés Castro Bianchi",
  "Erik Manuel Escalona Avendaño",
  "Esteban Othoniel Montenegro Guerrero",
  "Francisco José Valderrey Barreal",
  "Guillermo Carlos De La Rosa",
  "Jessica Freire Ponte",
  "Joffre Antonio Arias Márquez",
  "Jon Martínez Bidezábal",
  "Juan Carlos Vez Vázquez",
  "Karol Bracho Yanez",
  "Luis Javier Rodríguez Martínez",
  "Manuel Nicolas González Konstantinoff",
  "Marco Antonio Eirín López",
  "Marcos Martínez Ferreño",
  "Maria de las Mercedes Iñiguez Quintela",
  "María Isabel Abad Camiños",
  "Mario Jesús Rodríguez Rodríguez",
  "Matteo María  Stella",
  "Maximiliano Antonio Vilariño Fernández",
  "Mladen Aleksandro",
  "Nicolás Caramés García",
  "Pablo Planes Pedreño",
  "Rodrigo Martin Acchini",
  "Rubén Taibo Otero",
  "Sara Hurtado Kebschull",
  "Víctor Otero Vidal",
  "Weslley Castro Santos",
  "Stefano Peraldini",
];
const studentsPerGroup = 4;
let groups = []; //aquí iré agregando los grupos formados

console.log(students.length); //TODO console.log

//El while externo me crea grupos mientras existan suficientes estudiantes en la lista
let position = 0;
while (students.length >= studentsPerGroup) {
  groups[position] = []; //creo dinámicamente un array vacío dentro de cada posición en el array groups
  //En el siguiente while creo los grupos escogiendo aleatoriamente a los estudiantes
  while (groups[position].length < studentsPerGroup) {
    let rndNum = Math.floor(Math.random() * students.length);
    groups[position].push(students[rndNum]); //inserto un estudiante aleatorio en el nuevo grupo
    students.splice(rndNum, 1); //elimino de estudiantes al alumno que acabo de asignar a un grupo para que no se repita en otros grupos
  }
  position++; //incremento para almacenar un nuevo grupo en la siguiente posición del array groups
}
console.log(`Estudiantes sin asignar: ${students}`); //TODO console.log muestro los estudiantes sin grupo
console.log("*****************************************") //TODO console.log

//hago un bucle similar al anterior para asignar aleatoriamente a los rezagados a un grupo
let i = 0;
while (students.length) {
  let rndNum = Math.floor(Math.random() * students.length);
  groups[i].push(students[rndNum]);
  console.log(`Asigno a ${students[rndNum]} al grupo ${i + 1}`); //TODO console.log
  students.splice(rndNum, 1);
  i++;
}
console.log("*************************"); //TODO console.log

//itero sobre los grupos para mostrar los participantes en cada grupo
for (let j = 0; j < groups.length; j++) {
  console.log(`Grupo${j + 1} (${groups[j].length} estudiantes): ${groups[j]}`);
}

console.log("*************************"); //TODO console.log
console.log(`Hay ${students.length} estudiantes sin asignar`); //TODO console.log
