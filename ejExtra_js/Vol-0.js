"use strict"
// //EJERCICIO 1
//Programa que permite leer la edad y peso de una persona y posteriormente la imprime
// const edad = prompt("Ingresa tu edad");
const edad = 35;
const peso = 55;
console.log(`EJ-1: Tienes ${edad} años de edad y pesas ${peso}kg`);

//EJERCICIO 2
//Programa que dado dos valores de entrada imprime siempre 
//la división del mayor entre el menor
const ej2Num1 = 3;
const ej2Num2 = 30;
let resultado;
if (ej2Num1 >= ej2Num2) {
  resultado = ej2Num1 / ej2Num2;
}
else {
  resultado = ej2Num2 / ej2Num1;
}
console.log(`EJ-2: ${resultado}`);

//ej2 con Math.max y Math.min
console.log(`EJ2-v2: ${Math.max(ej2Num1, ej2Num2) / Math.min(ej2Num1, ej2Num2)}`);

//EJERCICIO 3
//Programa que lee 3 numeros e indica cuál es el mayor
const ej3Num1 = 91;
const ej3Num2 = 85;
const ej3Num3 = 91;
let ej3NumMayor = 0;

if (ej3Num1 >= ej3Num2 && ej3Num1 >= ej3Num3) {
  ej3NumMayor = ej3Num1;
}
else if (ej3Num2 >= ej3Num1 && ej3Num2 >= ej3Num3) {
  ej3NumMayor = ej3Num2;
}
else {
  ej3NumMayor = ej3Num3;
}
console.log(`EJ-3: {${ej3Num1}, ${ej3Num2}, ${ej3Num3}} => El número mayor es ${ej3NumMayor}`);

//EJERCICIO 4
//Programa que lee 3 números=(día/mes/año). Comprobar que sea válida la fecha.
//Si no es válida imprimir mensaje de error.
//Si es válida, imprimir la fecha (el mes con su nombre)
const day = 23;
let month = 12;
const year = 1987;
// let month = "";
let fecha = "dd/mm/yyyy";
if ((day > 0 && day <= 31) && (month >= 1 && month <= 12)
  && (year >= 0 && year <= 2030)) {
  switch (month) {
    case 1: month = "Enero"; break;
    case 2: month = "Febrero"; break;
    case 3: month = "Marzo"; break;
    case 4: month = "Abril"; break;
    case 5: month = "Mayo"; break;
    case 6: month = "Junio"; break;
    case 7: month = "Julio"; break;
    case 8: month = "Agosto"; break;
    case 9: month = "Septiembre"; break;
    case 10: month = "Octubre"; break;
    case 11: month = "Noviembre"; break;
    case 12: month = "Diciembre"; break;
  }
  fecha = `EJ-4: La fecha es ${day}/${month}/${year}`;
}
else {
  fecha = "EJ-4: Fecha no válida";
}
console.log(fecha);

//EJERCICIO 5
//Programa que pide la edad, si es mayor de edad puede votar, si es menor no puede.
const mayoriaEdad = 18;
const ej4Edad = 13;
let ej4Resultado;
if (ej4Edad >= mayoriaEdad) {
  ej4Resultado = "Puede votar";
} else {
  ej4Resultado = "No puede votar";
}
console.log(`EJ-5: ${ej4Resultado}`);

//EJERCICIO 6
//Programa lee un número entero y visualiza si es par o impar
//Si es 0 debe decir "el num no es par ni impar"
const ej6Valor = 200;
let ej6Resto;
let ej6Resultado;
if (ej6Valor === 0) {
  ej6Resultado = "no es par ni impar"
} else {
  ej6Resto = ej6Valor % 2;
  if (ej6Resto === 0) {
    ej6Resultado = "es par";
  } else {
    ej6Resultado = "es impar";
  }
}
console.log(`EJ-6: El número ${ej6Valor} ${ej6Resultado}.`);




