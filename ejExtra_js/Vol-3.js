"use strict"
/* **Ejercicio 1
Pide la edad y si es mayor de 18 años indica que ya puede conducir.
Pista: Se pueden usar diálogos (prompt) o directamente se asignan
las variables. */
// const mayoriaEdad = 18;
// let edad = 20;
// console.log(`${edad >= mayoriaEdad ? "Puede conducir" : "No puede conducir"}`);


/* **Ejercicio 2
Pedimos un precio (sin IVA) y calculamos el precio con IVA del 21%. */
// const iva = 21;
// const calcularConIVA = (precio) => {
//   return precio += (precio * (iva / 100));
// }
// let precio = 560;
// console.log(`El precio con iva es: ${calcularConIVA(precio)}€`);


/* **Ejercicio 3
Pide una nota (número). Muestra la calificación según la nota (por
console o por dialogo alert). Calificación:
0-3: Muy deficiente
3-5: Insuficiente
5-6: Suficiente
6-7: Bien
7-9: Notable
9-10: Sobresaliente
Pista: Se pueden usar diálogos (prompt) o directamente se asignan las variables. */
// const nota = 10;
// let result;
// switch (nota) {
//   case 0:
//   case 1:
//   case 2:
//     result = `${nota} - Muy deficiente`;
//     break;
//   case 3:
//   case 4:
//     result = `${nota} - Insuficente`;
//     break;
//   case 5:
//     result = `${nota} - Suficiente`;
//     break;
//   case 6:
//     result = `${nota} - Bien`;
//     break;
//   case 7:
//   case 8:
//     result = `${nota} - Notable`;
//     break;
//   case 9:
//   case 10:
//     result = `${nota} - Sobresaliente`;
//     break;

//   default:
//     result = "Nota no válida."
//     break;
// }
// console.log(result);


/* **Ejercicio 4 (esperar de ver los metodos de los arrays)
Realiza un script que pida cadenas de texto (en un dialogo prompt) hasta que se escriba
“cancelar”. Al salir con “cancelar” deben mostrarse todas las cadenas concatenadas con un guión. */
// let cancelar = false;
// let result = [];
// do {
//   const userIn = prompt("Ingresa un texto o escribe cancelar para salir");
//   if (userIn.toLowerCase() === "cancelar") {
//     cancelar = true;
//   } else {
//     result.push(userIn);
//   }
// } while (cancelar == false);
// const resultToString = result.join("-");
// alert(resultToString);


/* **Ejercicio 5 (esperar de ver los metodos de los arrays)
Realiza un script que pida números hasta que se pulse “cancelar”. Si no es un número
deberá indicarse con un «alert» y seguir pidiendo. Al salir con “cancelar” deberá
indicarse la suma total de los números introducidos. */
// let isFinish = false;
// let arrayNums = [];
// let sum;
// do {
//   const userIn = prompt("Ingresa un número: ");
//   if (userIn === null) {
//     isFinish = true;
//   }
//   else if (Number(userIn)) {
//     const num = parseInt(userIn);
//     arrayNums.push(num);
//   }
//   else {
//     alert("Introduce un número o click en cancelar para salir");
//   }

// } while (isFinish === false);

// if (arrayNums.length > 0) {
//   sum = arrayNums.reduce((acc, value) => {
//     return acc + value;
//   });
//   alert(`La suma es: ${sum}`);
// }

/* **Ejercicio 6 (esperar de ver los metodos de los arrays)
Realiza un script que pida número de filas y columnas y escriba una tabla. Dentro cada
una de las celdas deberá escribirse un número consecutivo en orden descendente.
Si, por ejemplo, la tabla es de 7×5 los números irán del 35 al 1. */
// let isFinish = false;
// do {
//   let numFilas = parseInt(prompt("Cuántas filas?"));
//   let numColumnas = parseInt(prompt("Cuántas columnas?"));
//   if (isNaN(numFilas) || isNaN(numColumnas) || numFilas <= 0 || numColumnas <= 0) {
//     alert("Ingresa números enteros mayores de cero");
//   } else {
//     let numMax = numFilas * numColumnas;
//     for (let i = 0; i < numFilas; i++) {
//       let lineResult = "";
//       for (let j = 0; j < numColumnas; j++) {
//         lineResult += numMax-- + " ";
//       }
//       console.log(lineResult.trim());
//     }
//     isFinish = true;
//   }
// } while (!isFinish);


/* **Ejercicio 7 (se puede hacer con array o no)
Genera 3 números aleatorios entre 1 y 99 pero que no se repita ninguno */
// let numQty = 3;
// let min = 1;
// let max = 99;
// let array = [];
// while (numQty > 0) {
//   const rdnNum = Math.floor(Math.random() * (max - min + 1) + min);
//   if (!array.includes(rdnNum)) {
//     array.push(rdnNum);
//   }
//   numQty--;
// }
// console.log(array);

/* **Ejercicio 8
Realiza un juego de Piedra Papel Tijera contra el Pc. Se le pregunta al
usuario que va a jugar (piedra-papel o tijera) mediante un dialogo
prompt. El ordenador realizará una jugada aleatoria (igualmente
piedra-papel o tijera). Cada vez que gane uno se le apuntará 1 punto.
En caso de empate ninguna de las 2 partes sumará puntos. El
ganador final será el que consiga 3 puntos. */
let userScore = 0;
let botScore = 0;
const array = ["piedra", "papel", "tijera"];
let isFinish = false;
let userChoice;

do {
  const userIn = prompt("piedra, papel o tijera??");
  const rdnNum = Math.floor(Math.random() * array.length);
  let botTurn = array[rdnNum];
  if (userIn != null) {
    userChoice = userIn.toLowerCase();
  }
  switch (botTurn) {
    case "piedra":
      if (userChoice === "papel") {
        userScore++;
      } else if (userChoice === "tijera") {
        botScore++;
      }
      break;
    case "papel":
      if (userChoice === "tijera") {
        userScore++;
      } else if (userChoice === "piedra") {
        botScore++;
      }
      break;
    case "tijera":
      if (userChoice === "piedra") {
        userScore++;
      } else if (userChoice === "papel") {
        botScore++;
      }
      break;
  }

  alert(`Robot ${botScore} - ${userScore} Tú`);
  if (userScore === 3 || botScore === 3) {
    if (userScore > botScore) {
      alert("Has ganado!!");
    }
    else {
      alert("Intentalo nuevamente!");
    }

    isFinish = true;
  }
} while (!isFinish);
