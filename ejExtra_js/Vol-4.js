/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Escribe un programa que permita al usuario introducir elementos en un array.
 * El programa finalizará cuando el usuario introduzca el string "fin", y se
 * mostrará por consola el contenido del array.
 *
 */
// let array = [];
// let isFinish = false;
// while (!isFinish) {
//   const userIn = prompt("Introduce elementos o escribe fin para finalizar");
//   if (userIn.toLowerCase() != "fin") {
//     array.push(userIn);
//   }
//   else {
//     console.log(array);
//     isFinish = true;
//   }
// }

/**
* #################
* ## Ejercicio 2 ##
* #################
*
* Dado el array [3, 4, 13, 5, 6, 8], muestra por consola qué numeros son pares
* y qué números son impares.
*
* Haz lo mismo pero en este caso indica qué números son primos y cuales no.
*
* Por último, crea un nuevo array en el que los valores sean el doble del array
* original.
*
*/

const array = [3, 4, 0, 13, 5, 6, 8];
// function esPar(array) {
//   for (let i = 0; i < array.length; i++) {
//     if (array[i] === 0) {
//       console.log(`${array[i]} No es par ni impar`);
//     }
//     else if (array[i] % 2 === 0) {
//       console.log(`${array[i]} es par`);
//     }
//     else {
//       console.log(`${array[i]} es impar`)
//     }
//   }
// }
// esPar(array);

// function esPrimo(array) {
//   // num % num === 0 && num % 1 === 0; números primos, divisible solo por 1 y por sí mismo
//   // num <= 1; no son primos

//   for (let i = 0; i < array.length; i++) {
//     let result = "es primo";
//     if (array[i] <= 1) {
//       result = "no es primo";
//     } else {
//       for (let j = 2; j < array[i]; j++) {
//         if (array[i] % j === 0) {
//           result = "no es primo";
//           break;
//         }
//       }
//     }
//     console.log(`${array[i]} => ${result}`);
//   }
// }
// esPrimo(array);
//map function devolviendo el array doble
// console.log(array);
// console.log(array.map((elemento) => {
//   return elemento * 2;
// }));


/**
* #################
* ## Ejercicio 3 ##
* #################
*
* Hemos visto como podemos recorrer automáticamente un array empleando el bloque
* for y la propiedad nombreArray.length. Pues de igual modo que recorremos arrays
* es posible recorrer strings utilizando ese mismo método.
*
* Tomemos como referencia el string "JavaScript mola mucho". En este ejercicio
* deberás mostrar por consola el número total de caracteres que tiene este string
* SIN CONTAR los espacios. El resultado debería ser 19 para el string anterior.
*
*/

// let string = "JavaScript mola mucho";
// console.log(string.split(" ").join("").length);

/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * - Crea el objeto coche y asígnale las propiedades modelo, marca y color.
 *
 * - Muestra el objeto por medio de un "console.log".
 *
 * - Modifica el valor de la propiedad color y agrega la propiedad año de
 *   matriculación.
 *
 * - Utiliza un "confirm" para mostrar por consola las propiedades, o los
 *   valores. Si la persona acepta el "confirm" se mostrarán las propiedades,
 *   de lo contrario, se mostrarán los valores.
 */
// const coche = {
//   modelo: "MX-5",
//   marca: "Mazda",
//   color: "rojo"
// }
// console.log(coche);
// coche.color = "azul";
// coche.yearMatricula = 2023;
// console.log(coche);

// const userIn = confirm("Aceptar para ver las propiedades o Cancelar para ver los valores");
// if (userIn) {
//   alert(Object.keys(coche).toString().split(",").join(", "));
// } else {
//   alert(Object.values(coche).toString().split(",").join(", "));
// }


/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Crea una función que reciba una cadena de texto y la muestre poniendo
 * el signo – entre cada carácter sin usar el método replace ni replaceAll.
 *
 * Por ejemplo, si tecleo "hola qué tal", deberá salir "h-o-l-a- -q-u-e- -t-a-l".
 *
 */

// const string = "hola qué tal";
// console.log(string.split("").join("-"));

// //ahora con replace
// const newstring = string.replaceAll("", "-").split("");
// newstring.shift();
// newstring.pop();
// console.log(newstring.join(""));


/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Crea una función que interprete el contenido entre paréntisis de
 * un string dado.
 *
 *      - El programa debe devolver el texto rodeando por paréntisis.
 *
 *      - Si no hay paréntisis se devolverá un string vacío.
 *
 *      - Si existe paréntisis de apertura pero no de cierre se devolverá
 *        el contenido desde el primer paréntisis hasta el final del string.
 *
 *      - Si existe paréntisis de cierre pero no de apertura se devolverá
 *        el contenido desde el inicio hasta el paréntisis de cierre.
 *
 * Por ejemplo, si el string fuera "Hola (que) tal" se mostrará el "que".
 *
 * Si fuera "Hola (que tal" se devolvería "que tal".
 *
 */
// const text = "Hola (que) tal como estás tu";
// const text1 = "Hola que) tal como estás tu";
// const text2 = "Hola (que tal como estás tu";
// const text3 = "Hola que tal como estás tu";
// function leerTexto(txt) {
//   let newarray = [];
//   const opener = txt.indexOf("(");
//   const closure = txt.lastIndexOf(")");
//   txt.split("").forEach((element, i) => {
//     if (opener != -1 && closure != -1) {
//       if (i > opener && i < closure) {
//         newarray.push(element);
//       }
//     } else if (closure === -1) {
//       if (opener != -1 && i > opener) {
//         newarray.push(element);
//       }
//     } else if (opener === -1) {
//       if (closure != -1 && i < closure) {
//         newarray.push(element);
//       }
//     }
//   });
//   if (newarray.length != 0) {
//     return newarray.join("");
//   }
//   return newarray;

// }
// console.log(`${text} =>`, leerTexto(text));
// console.log(`${text1} =>`, leerTexto(text1));
// console.log(`${text2} =>`, leerTexto(text2));
// console.log(`${text3} =>`, leerTexto(text3));


/**
//  * #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea una función que pida una cadena de texto y la devuelva al revés.
 * Es decir, si tecleo "hola que tal" deberá mostrar "lat euq aloh".
 *
 */
const txt = "hola que tal";
console.log(txt.split("").reverse().join(" "));


/* #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Crea un programa que compruebe si un string es palíndromo, es decir, que se lee igual
 * del derecho que del revés. Por ejemplo, "Arriba la birra" es un palíndromo.
 *
 */


/* #################
 * ## Ejercicio 9 ##
 * #################
 *
 * - Cuenta el número de letras "r" en el siguiente fragmento de texto:
 *   "Tres tristes tigres tragan trigo en un trigal."
 *
 * - Ahora cuenta las "t". Debes contar las mayúsculas y las minúsculas.
 *
 * - Sustituye todas las "e" por "i".
 *
 */


/**
 * #################
 * ## Ejercicio 10 #
 * #################
 *
 * Dado un array de frutas, genera un nuevo objeto en el que cada fruta pase a ser una
 * nueva propiedad del objeto. El valor asignado a esta propiedad debe ser el nº de veces
 * que la fruta se repite en el array.
 *
 *      const fruitBasket = ['naranja', 'naranja', 'limón', 'pera', 'limón', 'plátano', 'naranja'];
 *
 * Para el array anterior, el objeto resultante debería ser:
 *
 *      const fruits = {
 *          naranja: 3,
 *          limón: 2,
 *          pera: 1,
 *          plátano: 1
 *      };
 */