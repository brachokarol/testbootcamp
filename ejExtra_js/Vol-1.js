"use strict"
/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una variable que almacene tu nombre y otra variable que almacene
 * tu número favorito.
 *
 *  - Muestra ambos valores por consola.
 *
 *  - Cambia tu nº favorito por cualquier otro nº.
 *
 *  - Muestra por consola el resultado del cambio.
 *
 *  - Muestra por consola el tipo de las variables definidas.
 *
 */
let nombre = "Karol";
let numFav = 23;
console.log(`Nombre: ${nombre} - Núm fav: ${numFav}`);
numFav = 8;
console.log(`Nuevo núm fav: ${numFav}`);
console.log(`Nombre es de tipo ${typeof (nombre)} y número favorito es de tipo ${typeof (numFav)}`);


/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Dado el valor "num_a = 8" y "num_b = 2":
 *
 *  - Muestra por consola la suma de A + B.
 *
 *  - Muestra el resto de dividir A entre B.
 *
 *  - Muestra el resultado de elevar A a la potencia de B.
 *
 */
const num_a = 8;
const num_b = 2;
console.log(`Suma a+b: ${num_a + num_b}`);
console.log(`Resto a%b: ${num_a % num_b}`);
console.log(`Potencia a^b: ${Math.pow(num_a, num_b)}`);

/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una calculadora con "if" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 *
 */
const num1 = 20;
const num2 = 4;
let option = "^";
let resultado;
let mensaje;
let esValido = true;
if (option === "+") {
  resultado = num1 + num2;
}
else if (option === "-") {
  resultado = num1 - num2;
}
else if (option === "*") {
  resultado = num1 * num2;
}
else if ((option === "/") && (num2 != 0)) {
  resultado = num1 / num2;
}
else if (option === "^") {
  resultado = Math.pow(num1, num2);
}
else {
  esValido = false;
}
if (esValido) {
  mensaje = `IF: El resultado de ${num1} ${option} ${num2} = ${resultado}`;
} else {
  mensaje = "No se puede divir entre cero";
}
console.log(mensaje);

/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea una calculadora con "switch" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 *
 */

const num1_s = 25;
const num2_s = 5;
let option_s = "*";
let resultado_s;

switch (option_s) {
  case "+":
    resultado_s = num1_s + num2_s;
    break;
  case "-":
    resultado_s = num1_s - num2_s;
    break;
  case "*":
    resultado_s = num1_s * num2_s;
    break;
  case "/":
    if (num2_s > 0) {
      resultado_s = num1_s / num2_s;
    } else {
      resultado_s = "No se puede divir entre 0";
    }
    break;
  case "^":
    resultado_s = Math.pow(num1_s, num2_s);
    break;
}
if (num2_s === 0) {
  console.log(`SWITCH: ${resultado_s}`);
} else {
  console.log(`SWITCH: El resultado de ${num1_s} ${option_s} ${num2_s} = ${resultado_s}`);
}

