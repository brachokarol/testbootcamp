"use strict"
/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una función que imprima X resultados aleatorios de una
 * quiniela 1 X 2. Ejemplo, si le decimos que imprima 14 resultados:
 *
 *      Resultado 1: 1
 *      Resultado 2: X
 *      Resultado 3: 2
 *      (...)
 *      Resultado 14: 2
 *
 * 1 => 1;  2 => 2;  3 => x
 */

// const quinela = (num) => {
//   for (let i = 1; i <= num; i++) {
//     let randomNum = Math.floor(Math.random() * 3) + 1;
//     console.log(`Resultado ${i}: ${randomNum === 3 ? "x" : randomNum}`);
//   }
// }
// quinela(14);




/**
 * #################
 * ## Ejercicio 1-B ##
 * #################
 *
 * Crea una función que imprima X resultados aleatorios de una
 * quiniela 1 X 2 con la probabilidad de que salga el 1 sea del 60%,
 * la de la X del 30% y la del 2 del 10%
 * Ejemplo, si le decimos que imprima 14 resultados:
 *      Resultado 1: 1
 *      Resultado 2: X
 *      Resultado 3: 2
 *      (...)
 *      Resultado 14: 2
 *
 */

// function generaQuiniela(numResultados) {
//   for (let i = 1; i <= numResultados; i++) {
//     let res = Math.floor(Math.random() * 10) + 1; // num entre 1 y 10

//     switch (res) {
//       case 1:
//       case 2:
//       case 3:
//       case 4:
//       case 5:
//       case 6:
//         res = 1;
//         break;
//       case 7:
//       case 8:
//       case 9:
//         res = "x";
//         break;
//       case 10:
//         res = 2;
//         break;
//     }

//     console.log(`Resultado ${i}: ${res}`);
//   }
// }
// generaQuiniela(14);




/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Realizar un algoritmo que calcule el valor de la letra de un número de DNI
 * (Documento nacional de indentidad).
 *
 * Buscar en Google como se genera la letra del DNI.
 *
 * Usad funciones para una mejor organización del código.
 */

// const generarLetraDNI = (digitosDNI) => {
//   const resto = digitosDNI % 23;
//   const letras = "TRWAGMYFPDXBNJZSQVHLCKE";
//   return letras[resto];
// }
// const dniNumber = 26590523;
// console.log(`DNI: ${dniNumber}-${generarLetraDNI(dniNumber)}`);




/* ################
 * ## Pirámide 1 ##
 * ################
 *
 * Utiliza el bucle "for" para crear las siguiente figura con asteriscos (*). Debe existir
 * una variable que permita modificar la altura de la pirámide. Para el ejemplo expuesto a
 * continuación se ha tomado como referencia una altura de 4:
 *
 * - Figura 1:
 *
 *      😁
 *      😁😁
 *      😁😁😁
 *      😁😁😁😁
 */
// const altura = 4;
// for (let i = 0; i <= altura; i++) {
//   console.log("😁".repeat(i));

// }




/**
* ################
* ## Pirámide 2 ##
* ################
*
* Crea una función que reciba una altura y dibuje una figura
* como la que sigue:
*
*    1
*    22
*    333
*    4444
*    55555
*
*/
const altura = 5;
for (let i = 1; i <= altura; i++) {
  console.log(`${i}`.repeat(i));
}



/**
* ################
* ## Pirámide 3 ##
* ################
*
* Crea una función que reciba una altura y dibuje una figura
* como la que sigue:
*
*    1
*    12
*    123
*    1234
*    12345
*
*/

// function drawPyramid(altura) {
//   let nivel = "";
//   for (let i = 1; i <= altura; i++) {
//     nivel += i;
//     console.log(nivel);
//   }
// }
// drawPyramid(5);


/*Piramide simétrica*/
// function creaPiramideSimetrica(numero) {
//   //Calcula el núm de espacios vacios necesarios para centrar
//   let nroDeEspacios = Math.floor(numero / 2);
//   //crea un array de espacios vacíos, con length determinado por nroDeEspacios
//   var separa = Array(nroDeEspacios).fill(" ");
//   //convierte el array de espacios vacíos en un string de espacios vacíos
//   let sepa = separa.join("");

//   //Muestra la punta de la piramide y la setea a 1 como string para que concatene y no sume los numeros
//   let palabra = "1";
//   console.log(sepa + palabra);

//   for (let i = 2; i <= numero; i += 2) {
//     sepa = sepa.replace(" ", ""); //quito un espacio vacío con cada iteración
//     palabra += i + "" + (i + 1);
//     console.log(sepa + palabra);
//   }
// }
// creaPiramideSimetrica(9);

