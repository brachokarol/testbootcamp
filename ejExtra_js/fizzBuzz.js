"use strict";

/*
La idea es escribir un programa que imprima los números del 1 al 100. 
Si el número es divisible por 3, el programa debe imprimir “Fizz” en su lugar. 
Si el número es divisible por 5, el programa debe imprimir “Buzz”. 
Si el número es divisible por ambos 3 y 5, el programa debe imprimir “FizzBuzz”.

Bonus: al ser divisible entre 7 debe salir la palabra “Woff”?
*/
let i = 0;
while (i <= 100) {
  let word = "";
  if (i % 3 === 0 && i % 5 === 0) {
    word = "FizzBuzz";
  } else if (i % 3 === 0) {
    word = "Fizz";
  }
  else if (i % 5 === 0) {
    word = "Buzz";
  }
  if (i % 7 === 0) {
    word += " " + "Woff";
  }
  console.log(i++, word);
}
