"use strict";

// "Empezamos con un array ([true, false, true, false, false, false, true, true]),
// las posiciones con false están sanas, las que tienen true son contagiosas.
// Necesitamos una función que nos diga los contagiados el día siguiente,
// cada enfermo contagiará a los que se encuentran justo a su lado."

const patients = [true, false, true, false, false, false, true, true];
function getNextDay(patients) {
  // const siguienteDia = patients.slice();
  const siguienteDia = [...patients];

  for (let i = 0; i < patients.length; i++) {
    if (patients[i]) {
      if (i != 0) {
        siguienteDia[i - 1] = true;
      }
      if (patients[i] != patients.length - 1) {
        siguienteDia[i + 1] = true
      }
    }
  }
  return siguienteDia;
}
console.log(getNextDay(patients));


// const result = [true, true, true, true, false, true, true, true];
