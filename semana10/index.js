/* CORS MODULES DE NODE */

/**
 * Modulo OS da información referente al sistema operativo
 * */
const os = require('os');
// console.log('Total RAM:', os.totalmem());
// console.log('RAM libre:', os.freemem());
// console.log('Host Name:', os.hostname());
// console.log('Directorio:', os.homedir());
// console.log('Directorio archivos temporales:', os.tmpdir());
// console.log('CPU arquitectura:', os.arch());
// console.log('CPUs:', os.cpus());
// console.log('Interfacez de red:', os.networkInterfaces());
// console.log('OS plataforma:', os.platform());
// console.log('Toda la info del Sistema Operativo:', os);

/**
* Modulo PATH provee información sobre el directorio del proyecto
* Garantiza el buen funcionamiento del mismo
* Puedo setear las rutas
*/
//Variables de entorno
// console.log(__dirname); //devuelve la ruta ABSOLUTA del directorio
// console.log(__filename); //devuelve la ruta ABSOLUTA del directorio más el archivo


// //modulo PATH
const path = require('path');
// //join permite construir una ruta específica
// const dirProyecto = path.join(__dirname);
// console.log(dirProyecto);
// console.log(path.join('./semana10')); //es la misma ruta que guardamos en la variable dirProyecto

/**
* Modulo fs (file system)
* permite manejar archivos (leer, escribir, crear, eliminar, modificar)
*/
// const fs = require('fs');

//Crear un archivo
// fs.writeFile(nombre del archivo, la información, función callback para manejar errores)
//Si el archivo ya existe lo sobreescribe.
// fs.writeFile("prueba.txt", "Hola mundo desde JS", (err) => {
//   if (err) {
//     console.log(`Error: ${err.message}`);
//   } else {
//     console.log("El archivo se creó correctamente");
//   }
// });

//Leer un archivo
const fs = require('fs').promises;
const leer = async (file) => {
  try {
    const datos = await fs.readFile(file, 'utf8');
    console.log(datos);
  } catch (error) {
    console.log(error);
  }
}
const archivo = path.join(__dirname, 'prueba.txt');
leer(archivo);

