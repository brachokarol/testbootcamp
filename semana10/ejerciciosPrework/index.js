// probar con => node index.js --name=Karol
// probar con => node index.js
const prompt = require('prompt-sync')();
const parseArgs = require('minimist')(process.argv.slice(2));


// const values = Object.values(parseArgs);
// if (values[1] != undefined) {
//   const name = values[1];
//   console.log(`Hola ${name}!!`);
// }
// else {
//   console.log("Hola");
// }
const name = parseArgs.name;
name ? console.log(`Hola ${name}!`) : console.log("Hola!");

let isFinish = false;
do {
  const greeting = prompt("¿Qué tal estás? -- ");
  let mensaje = "";
  switch (greeting.toLowerCase()) {
    case "excelente":
    case "bien":
    case "de maravilla":
    case "absolutamente bien":
    case "mejor que bien":
      mensaje = "Me alegro mucho por tí!!!";
      isFinish = true;
      break;
    case "mal":
    case "mas o menos":
    case "no muy bien":
    case "estoy triste":
      mensaje = "Lo lamento mucho. Estoy aquí para ti!!";
      isFinish = true;
      break;
    default:
      mensaje = "Lo siento, no te he entendido";
      break;
  }
  console.log(mensaje);
} while (!isFinish);

process.on("uncaughtException", (error) => {
  console.log("Error inesperado. Vuelve a intentarlo!!");
  console.log(error.message);
});

process.exit(0);

