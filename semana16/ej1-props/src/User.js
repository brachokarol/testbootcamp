import { UserLocation } from "./UserLocation"
import { UserName } from "./UserName"
import { UserPicture } from "./UserPicture"
import "./User.css"

export const User = ({ user }) => {
  const age = user.dob.age;
  return (
    <article className="user">
      <UserPicture picture={user.picture} name={user.name} />
      <UserName name={user.name} />
      <UserLocation location={user.location} />
      {age > 18 ? <span className="age-tag">Mayor de edad</span> : null}
    </article>
  )
}