import './App.css';
import { UserList } from './UserList';

function App() {
  return (
    <main className="App">
      <UserList />
    </main>
  );
}

export default App;
