import { User } from "./User"
import users from "./users.json"
import "./UserList.css"
console.log(users);

export const UserList = () => {
  return <ul className="user-list">
    {users?.map((user) => {
      return <li key={`${user.id.value} + ${user.login.uuid}`} ><User user={user} /></li>
    })}
  </ul>
}