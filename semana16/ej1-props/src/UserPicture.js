export const UserPicture = ({ picture, name }) => {
  return (
    <img src={picture.large} alt={`Photo of ${name}`} className="user-picture" />
  )
}