import "./UserLocation.css"
export const UserLocation = ({ location }) => {
  return (
    <address className="user-location">
      <h3>Address</h3>
      <p>
        {`${location.street.name} ${location.street.number}.
          ${location.city}, ${location.country}.`}
      </p>
    </address>
  )
}