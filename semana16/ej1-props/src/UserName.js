export const UserName = ({ name }) => {
  return (
    <h2 className="user-name">{`${name.last}, ${name.first}`}</h2>
  )
}