import { Welcome } from "./components/Welcome";
import Table from "./components/Table";
import Gallery from "./components/Gallery";
import ContactCard from "./components/ContactCard";
import Acordeon from "./components/Acordeon";
import TodoList from "./components/TodoList";
import Counter from "./components/Counter";
import HookUseState from "./components/HookUseState";
import EventHandler from "./components/EventHandler";
import SignUp from "./components/SignUp";
import Formulario from "./components/Formulario";
import BlogComment from "./components/BlogComment";
import SizeSelect from "./components/SizeSelect";
import HookMyRef from "./components/HookMyRef";

function App() {
  const heading = <h2 className="header">This is React!!</h2>
  const obj = {
    id: 2,
    age: 35,
    name: "Karol"
  }
  return (
    <div className="App">
      <h1>Ejemplos de Cooding Room</h1>
      <Acordeon title="Primer componente">
        <Welcome name="Karol" />
      </Acordeon>

      <Acordeon title="Imprimir elementos en una constante">
        {heading}
      </Acordeon>

      <Acordeon title="Imprimiendo objetos">
        <p>Usando JSON.stringify = {JSON.stringify(obj)}</p>
        <p>Propiedad name = "{obj.name}"</p>
        <p style={{ color: "red", fontSize: "10px" }}>No se pueden imprimir objetos directamenta en react</p>
      </Acordeon>

      <Acordeon title="Ver tabla">
        <Table />
      </Acordeon>

      <Acordeon title="Abrir Galería">
        <Gallery />
      </Acordeon>

      <Acordeon title="Agenda de contactos">
        <ContactCard name="Jason Momoa" phone="+1 808 656 5667" mail="elmomoa@jmomoa.com" />
        <ContactCard name="Julia Roberts" phone="+1 808 656 5457" mail="lajuly@jroberts.com" />
        <ContactCard name="Al Pacino" phone="+1 234 656 5667" mail="elpadrino@alpacino.com" />
        <ContactCard phone="+1 234 654 5667" mail="unknown@unknown.com" />
      </Acordeon>

      <Acordeon title="Lista de tareas">
        <TodoList />
      </Acordeon>

      <Acordeon title="Contador de clicks">
        <Counter />
      </Acordeon>

      <Acordeon title="UseState">
        <HookUseState />
      </Acordeon>

      <Acordeon title="event handlers">
        <EventHandler />
      </Acordeon>

      <Acordeon title="Sign Up">
        <SignUp />
      </Acordeon>

      <Acordeon title="Objeto form en useState">
        <Formulario />
      </Acordeon>

      <Acordeon title="BlogComment">
        <BlogComment />
      </Acordeon>

      <Acordeon title="Input Select">
        <SizeSelect />
      </Acordeon>

      <Acordeon title="Hook MyRef">
        <HookMyRef />
      </Acordeon>



    </div>
  );
}

export default App;
