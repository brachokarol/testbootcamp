import { useState } from "react"

export default function SizeSelect() {
  const [size, setSize] = useState("");
  return (
    <>
      <select name="size" value={size} onChange={(e) => {
        setSize(e.target.value);
      }}>
        <option value="xs">Extra Small</option>
        <option value="sm">Small</option>
        <option value="md">Medium</option>
        <option value="lg">Large</option>
        <option value="xl">Extra Large</option>
      </select>
      <p>You selected: {size}</p>
    </>
  )
}