import { useState } from "react"

export default function Formulario() {
  const [formState, setFormState] = useState({ username: "", email: "" })

  //destructuring del target para acceder a los inputs
  const handleInputChange = ({ target }) => {
    //sacar el name y el value del target
    const { name, value } = target
    //modificamos la propiedad name con su valor en el formState actual
    setFormState({ ...formState, [name]: value })
  }
  return (
    <>
      <h1>Formulario simple con email y nombre</h1>
      <form>
        <input
          type="text"
          name="username"
          value={formState.username}
          onChange={handleInputChange}
          placeholder="Introduce tu nombre" />
        <input
          type="email"
          name="email"
          value={formState.email}
          onChange={handleInputChange}
          placeholder="Introduce tu email" />
      </form>
      <p>
        Valor del campo <em>username</em> en <code>formState</code>:{formState.username}
      </p>
      <p>
        Valor del campo <em>email</em> en <code>formState</code>:{" "}{formState.email}
      </p>
    </>
  )

}