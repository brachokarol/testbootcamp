// Componente ButtonAction

function ButtonAction({ handleClick, children }) {
  return <button onClick={handleClick}>{children}</button>;
}

// Componente App

export default function EventHandler() {
  return (
    <>
      <ButtonAction handleClick={() => alert('Autodestrucción iniciada 💣')}>
        Autodestrucción
      </ButtonAction>
      <ButtonAction handleClick={() => alert('Autodestrucción detenida 🛑')}>
        Detener Autodestrucción
      </ButtonAction>
    </>
  );
}