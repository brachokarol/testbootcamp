import { useRef } from "react"
// export default function HookMyRef() {
//   const myRef = useRef(null)
//   const verRef = () => {
//     console.log(myRef);
//   }
//   return (
//     <p ref={myRef} onClick={verRef}>Hola Ref</p>
//   )
// }

export default function HookMyRef() {
  const myRef = useRef(null);
  const handleFocus = (e) => {
    e.preventDefault()
    myRef.current.focus();

  }
  return (
    <div>
      <h1>Sobre Refs</h1>
      <form>
        <label>Usuario:
          <input type="text" name="username" />
        </label>
        <label>Email:
          <input type="email" name="email" ref={myRef} />
        </label>
        <button onClick={handleFocus}>Focus</button>
      </form>

    </div>

  )
}