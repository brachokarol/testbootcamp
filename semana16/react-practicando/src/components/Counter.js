import { useState } from "react"

export default function Counter() {
  const [counter, setCounter] = useState(0)
  const handlerSubmit = (e) => {
    setCounter(counter + 1)
  }
  return (
    <button onClick={handlerSubmit}>Click here {counter}</button>
  )
}