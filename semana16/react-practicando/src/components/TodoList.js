import { useState } from "react"

export default function TodoList() {
  const [item, setItem] = useState("")
  const [todoList, setTodoList] = useState(["oso", "nena"])
  const handleSubmit = (e) => {
    e.preventDefault();
    const list = [...todoList, item]
    setTodoList(list)
    setItem("")
  }
  const removeTodo = (todo) => {
    setTodoList(todoList.filter(x => x !== todo))
  }

  const currentList = todoList?.map((todo, index) => {
    return <li key={index}>{todo} <button id="papelera" onClick={() => removeTodo(todo)}>🗑</button></li>
  })

  return (
    <div className="todoList">
      <form>
        <input
          type="text"
          value={item}
          onChange={(e) => setItem(e.target.value)}
          id="todoitem"
          placeholder="what to do?"
        />
        <button onClick={handleSubmit}>Add</button>
      </form>
      <ul>
        {currentList}
      </ul>
    </div>
  )
}