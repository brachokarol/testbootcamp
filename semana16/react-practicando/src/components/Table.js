import TableBody from "./TableBody"
import TableHeader from "./TableHeader"

const Table = () => {
  return (
    <table className="table table-striped" >
      <TableHeader />
      <TableBody />
    </table>
  )
}
export default Table