export default function ContactCard({ name = "Unknown", phone, mail }) {
  return (
    <article style={{
      backgroundColor: "cornsilk", paddingBottom: "10px"
    }}>
      <h2>✅{name}</h2>
      <h3>📱{phone}</h3>
      <h4>📧{mail}</h4>
    </article >
  )
}