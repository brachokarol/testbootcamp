const TableBody = () => {
  return (
    <tbody>
      <tr>
        <td>Zoe</td>
        <td>Teacher</td>
      </tr>
      <tr>
        <td>David</td>
        <td>Teacher</td>
      </tr>
      <tr>
        <td>Samuel</td>
        <td>Teacher</td>
      </tr>
      <tr>
        <td>Berto</td>
        <td>Cordinator</td>
      </tr>
    </tbody>
  )
}
export default TableBody