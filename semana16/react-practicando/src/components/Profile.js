export default function Profile(props) {
  return (
    <article className="profile-card">
      <h3>Imagen de {props.name}</h3>
      <img
        src={props.url}
        alt={`Imagen de ${props.name}`}
        style={{ width: 150 }}
      />
    </article>
  )
}