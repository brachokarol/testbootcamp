import { useState } from "react"
export default function HookUseState() {
  const [isDone, setIsDone] = useState(false)
  return (
    <p className="task" onClick={() => {
      setIsDone(!isDone)
    }} style={isDone ? { textDecoration: 'line-through' } : null}>Comprar pan</p>
  )
}