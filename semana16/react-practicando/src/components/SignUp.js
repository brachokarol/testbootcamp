import { useState } from "react"
export default function Signup() {
  const [email, setEmail] = useState("");

  const handleChange = (e) => {
    setEmail(e.target.value);
  }
  return (
    <>
      <form>
        <label> Email
          <input
            type="email"
            value={email}
            onChange={handleChange}
            id="email" />
        </label>
      </form>
      <p>
        Tu dirección de email es: <strong>{email}</strong>
      </p>
    </>
  )
}