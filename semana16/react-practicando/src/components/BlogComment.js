//ej de inputs no controlados (sin value)
import { useRef, useState } from "react";

export default function BlogComment() {
  const [comments, setComments] = useState([]);
  const textAreaRef = useRef(null);
  const recordComment = (e) => {
    e.preventDefault();
    setComments([...comments, textAreaRef.current.value]);
  }

  const commentList = comments.map((comment, index) => {
    return (
      <p key={index}>{comment}</p>
    )
  })

  return (
    <>
      <form onSubmit={recordComment}>
        <p>Introduce tu comentario:</p>
        <textarea ref={textAreaRef} />
        <button>Enviar comentario</button>
      </form>
      <section>
        <p>Todos los comentarios:</p>
        {commentList}
      </section>
    </>
  )
}