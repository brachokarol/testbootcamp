import { ThemeContext } from "./ThemeContext";
/* Aquí empieza nuestro ContextProvider
   Definimos el Provider para nuestro Context, es importante mandar
   como props los children que pueda contener */
export const ThemeProvider = ({ children }) => {
  //uso del state para almacenar y modificar los datos de nuestro context
  const [theme, setTheme] = useState(false);
  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}