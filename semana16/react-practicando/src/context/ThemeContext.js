//importamos createContext de React
import { createContext, useState } from "react";

//Declaramos en una constante un nuevo Context
export const ThemeContext = createContext();