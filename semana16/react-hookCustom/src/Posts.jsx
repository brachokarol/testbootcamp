import { usePosts } from './hooks/api'


function Posts() {
  const posts = usePosts('https://8gag-api.anxoso.com/posts')

  return (
    <div>
      {posts?.map(post =>
        <div key={post.id}>
          <h2>{post.title}</h2>
          <img src={post.image} />
        </div>
      )}
    </div>
  )
}

export default Posts
