import { useProducts } from './hooks/api'
import './Products.css'

function Products() {
  const products = useProducts()

  return products && (
    <div>
      {products?.map(product =>
        <div className="product-card" key={product.id}>
          <img src={product.image} />
          <span>
            {product.title}
          </span>
        </div>
      )}
    </div>
  )
}

export default Products