import useCountDown from './hooks/useCountDown'

function Countdown() {
  const timerEnded = useCountDown()
  return timerEnded ? <h1>Se acabó el tiempo!</h1> : <div>Espera...</div>
}

export default Countdown
