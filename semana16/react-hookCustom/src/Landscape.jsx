import { useEffect, useState } from "react"

function Landscape() {
  const [isLandscape, setIsLandscape] = useState(window.innerWidth > window.innerHeight)

  useEffect(() => {
    const handler = () => setIsLandscape(window.innerWidth > window.innerHeight);
    window.addEventListener('resize', handler);
    return () => window.removeEventListener('resize', handler);
  }, [])

  return (
    <div>{isLandscape ? 'HORIZONTAL' : 'VERTICAL'}</div>
  )
}

export default Landscape
