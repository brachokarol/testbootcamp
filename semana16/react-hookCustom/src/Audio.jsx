import { useRef } from "react"

function Audio() {
  const ref = useRef()

  const handlePlay = () => {
    ref.current.play()
  }

  const handlePause = () => {
    ref.current.pause()
  }


  return (
    <>
      <audio ref={ref} src="https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3" />
      <button onClick={handlePlay}>Play</button><button onClick={handlePause}>Pause</button>
    </>
  )
}

export default Audio
