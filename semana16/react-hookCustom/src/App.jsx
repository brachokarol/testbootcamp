import './App.css'
import { useState } from 'react'
import Acordeon from './Acordeon'
import Counter from './Counter'
import Countdown from './Countdown'
import PokemonCard from './PokemonCard'
import Posts from './Posts'
import Products from './Products'
import Resolution from './Resolution'
import Landscape from './Landscape'
import Video from './Video'
import Audio from './Audio'
import TodoList from './TodoList'
import Tienda from './Tienda/App'

function App() {
  const [count, setCount] = useState(0)


  console.log('Render')

  return (
    <>
      <button onClick={() => setCount((count) => count + 1)}>
        count is {count}
      </button>
      <Acordeon title="Countdown">
        <Countdown />
      </Acordeon>
      <Acordeon title="Counter">
        <Counter />
      </Acordeon>
      <Acordeon title="Posts">
        <Posts />
      </Acordeon>
      <Acordeon title="Show Pokemon">
        <PokemonCard id="28" />
      </Acordeon>
      <Acordeon title="Lista Productos">
        <Products />
      </Acordeon>
      <Acordeon title="Resolution">
        <Resolution />
        <Landscape />
      </Acordeon>
      <Acordeon title="Video">
        <Video />
      </Acordeon>
      <Acordeon title="Audio">
        <Audio />
      </Acordeon>
      <Acordeon title="Todo List">
        <TodoList />
      </Acordeon>
      <Acordeon title="Tienda">
        <Tienda />
      </Acordeon>
    </>
  )
}

export default App
