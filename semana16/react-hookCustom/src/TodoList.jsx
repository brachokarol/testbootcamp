import { useState } from "react"
import useList from './hooks/useList'

function TodoList() {
  const { list, add, remove } = useList()
  const [newEntry, setNewEntry] = useState('')

  const handleSubmit = e => {
    e.preventDefault()
    add(newEntry)
    setNewEntry('')
  }

  return (
    <div className="todo-list">
      <ul>
        {list.map((entry, i) =>
          <li key={i}>
            {entry}
            <button onClick={() => remove(entry)}>🗑️</button>
          </li>
        )}
      </ul>
      <form onSubmit={handleSubmit}>
        <input placeholder="Añadir..." value={newEntry} onChange={e => setNewEntry(e.target.value)} />
        <button>Add</button>
      </form>
    </div>
  )
}

export default TodoList
