import { useRef } from "react"

function Video() {
  const ref = useRef()

  const handlePlay = () => {
    ref.current.play()
  }

  const handlePause = () => {
    ref.current.pause()
  }


  return (
    <>
      <video ref={ref} src="https://dl6.webmfiles.org/big-buck-bunny_trailer.webm" />
      <button onClick={handlePlay}>Play</button><button onClick={handlePause}>Pause</button>
    </>
  )
}

export default Video
