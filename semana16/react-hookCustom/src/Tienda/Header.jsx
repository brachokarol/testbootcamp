import './Header.css'

function Header({ username, isLoggedIn }) {

  return (
    <header>
      <h1>MiTienda</h1>
      {isLoggedIn
        ?
          <div className="user">
            {username}
          </div>
        :
          <button>
            Iniciar sesión
          </button>
      }
    </header>
  )
}

export default Header
