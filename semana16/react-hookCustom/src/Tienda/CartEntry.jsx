import PropTypes from 'prop-types'
import { useState } from "react"
import useList from '../hooks/useList'

function CartEntry({ product }) {
  const { list, add, remove } = useList("")
  const [newProduct, setNewProduct] = useState("")

  const handleSubmit = e => {
    e.preventDefault()
    add(newProduct)
    setNewProduct('')
  }

  return (
    <div key={product.id} className="cart-entry">
      <img src={product.image} />
      <span>
        {product.title}
      </span>
    </div>
  )
}

CartEntry.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    image: PropTypes.string,
    title: PropTypes.string
  })
}

export default CartEntry
