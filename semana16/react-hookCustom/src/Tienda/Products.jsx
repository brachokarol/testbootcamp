import ProductCard from './ProductCard'
import products from './products.json'
import './Products.css'

function Products() {

  return (
    <main>
      Productos:
      <div>
        {products.map(product =>
          <ProductCard key={product.id} product={product} />
        )}
      </div>
    </main>
  )
}

export default Products
