import './App.css'
import Cart from './Cart'
import Header from './Header'
import Products from './Products'

function App() {

  return (
    <div className="app">
      <Header username="Karol" isLoggedIn />
      <div className="body">
        <Products />
        <Cart />
      </div>
    </div>
  )
}

export default App
