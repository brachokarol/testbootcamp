import CartEntry from './CartEntry'
import products from './products.json'
import './Cart.css'

function Cart() {
  return (
    <aside>
      Mi carro:
      <div>
        {products?.map(product =>
          <CartEntry key={product.id} product={product} />
        )}
      </div>
    </aside>
  )
}

export default Cart
