import './PokemonCard.css'
import { usePokemon } from './hooks/api'

function PokemonCard({ id }) {
  const info = usePokemon(id)

  return info && (
    <div className="poke-card">
      <h1>{info.name}</h1>
      <img src={info.sprites.front_default} alt={info.name} />
      <a href={info.species.url} target='_blank'>URL</a>
    </div>
  );
}

export default PokemonCard