import { useState } from "react"

function useList() {
  const [list, setList] = useState([])

  const add = (entry) => setList([...list, entry])
  const remove = (entry) => setList(list.filter(x => x !== entry))

  return { list, add, remove }
}

export default useList
