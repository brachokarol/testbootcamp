import useFetch from "./useFetch";

export const useProducts = () => useFetch(`https://fakestoreapi.com/products`)
export const usePokemon = (id) => useFetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
export const usePosts = () => useFetch(`https://8gag-api.anxoso.com/posts`)