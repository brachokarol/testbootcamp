import { useEffect, useState } from "react";

function useFetch(url) {
  const [info, setinfo] = useState()
  useEffect(() => {
    const getData = async () => {
      const res = await fetch(url)
      const data = await res.json()
      setinfo(data)
    }
    getData()
  }, [url])
  return info
}

export default useFetch