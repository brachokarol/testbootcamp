import { useEffect, useState } from 'react'

export default function useCountDown() {
  const [timerEnded, setTimerEnded] = useState(false)

  useEffect(() => {
    console.log('Effect')
    const t = setTimeout(() => {
      console.log('Timer ended')
      setTimerEnded(true)
    }, 5000)
    return () => {
      console.log('Effect is gone')
      clearTimeout(t)
    }
  }, [])

  return timerEnded
}