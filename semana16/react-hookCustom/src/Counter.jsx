import useCounter from "./hooks/useCounter"

function Countdown() {
  const counter = useCounter()

  return <div>{counter}</div>

}

export default Countdown
