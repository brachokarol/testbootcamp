"use-strict" //esta instrucción me ayuda a detectar los errores en mi código
// /*
//   Vas al gym y solo puedes entrar si:
//   -Pagas la entrada, valor 10€ ó
//   -Tienes invitación
// */
// const feeEntrada = 10;
// let dinero = 8;
// let invitacion = false;
// let mensaje = "";

// dinero >= feeEntrada || invitacion === true ? mensaje = "Puedes entrar" : mensaje = "No Puedes entrar";
// console.log(mensaje);

// //CONVERSIONES IMPLICITAS
// //Nota el + en string no suma, concatena
// console.log(1 + 1 + "1"); //output: "21"
// console.log(1 + (1 + "1")); // output: "111"
// console.log("1" + 1 + 1); // output: "111"
// console.log("3" - 3); //output: 0
// console.log(true + true); //output: 2 ya que true=1 y false=0


// //SCOPE
// //imprimir las e con un do while
// const strEjemplo = "Clase de Java Script! By Stefano"
// let position = 0;
// do {
//   position = strEjemplo.indexOf("e", position);
//   if (position != -1) {
//     console.log("Posición 'e' con while:", position++);
//   }
// } while (position != -1);

// //BUCLE FOR
// for (let index = 0; index < strEjemplo.length; index++) {
//   if (strEjemplo[index] === "e") {
//     console.log("Posición 'e' con for:", index);
//   }
// }

// //con la declaración fuera de la variable se imprime la mitad de una piramde, ya que no se resetea dentro del bucle.
// // JSB18RT 0
// // JSB18RT 0 1
// // JSB18RT 0 1 2
// // y así consecutivamente
// let nombre = "JSB18RT";
// for (let i = 0; i <= 5; i++) {
//   nombre += " " + i;
//   console.log(nombre);
// }

// //impresiones, outputs
// //imprimir media piramide con dos bucles for. sin repeat()
// const altura = 4;
// for (let i = 1; i <= altura; i++) {
//   let strToPrint = "";
//   for (let j = 1; j <= i; j++) {
//     strToPrint += "*";
//   }
//   console.log(strToPrint);
// }

// //imprimir media piramide con un for y reapeat()
// for (let i = 1; i <= altura; i++) {
//   console.log("*".repeat(i));
// }

// //contador con setInterval y deteniendolo sin setTimeout
// let counter = 0;

// const idInterval = setInterval(() => {
//   console.log(counter++);
//   if (counter > 10) {
//     clearInterval(idInterval);
//   }
// }, 1000);

//Alarma que suena cuando pasan X segundos. También debe imprimir los segundos
// function alarm(secondToRing) {
//   let counter = 0;
//   const idInterval = setInterval(() => {
//     let secSecs = counter === 1 ? "segundo" : "segundos";
//     console.log(`${counter++} ${secSecs}`);
//     if (counter === secondToRing) {
//       console.log("RING RING RING");
//       clearInterval(idInterval);
//     }
//   }, 1000);
// }
// alarm(5);


// //testeando las salidas de bucles. Imprimir las vocales (No la posición)
// const vowels = ['a', 'e', 'i', 'o', 'u'];
// //con for/in
// for (let vowel in vowels) { console.log(vowels[vowel]) }

// //con for/of SOLO PARA ITERABLES
// for (let vowel of vowels) { console.log(vowel) }

// //con for tradicional
// for (let i = 0; i < vowels.length; i++) { console.log(vowels[i]) }

// ordenar un array sin Sort()
// let array = [101, 5, 30, 100, 50];
// console.log(array)
// for (let i = 0; i < array.length; i++) {
//   for (let j = i + 1; j <= array.length; j++) {
//     if (array[i] > array[j]) {
//       let aux = array[i];
//       array[i] = array[j];
//       array[j] = aux;
//     }
//   }
// }
// console.log(array);

/*Mezclar un array aleatoriamente con el algoritmo de Fisher–Yates.
Ver vídeo explicativo => https://www.youtube.com/watch?v=0eKNvuPNLos*/
function mezclarArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}
console.log(mezclarArray([1, 2, 3, 4, 5]));
