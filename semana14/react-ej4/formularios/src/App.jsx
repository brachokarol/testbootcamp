import { useState } from 'react'
import { Login } from './components/Login'
import { Signup } from './components/Signup'
import { Header } from './components/Header'
import { LoadData } from './components/LoadData/LoadData.jsx'
import './App.css'

function App() {
  const [user, setUser] = useState()
  const [isLogin, setIsLogin] = useState(true)

  return (
    <div>
      <Header user={user} />
      {!user &&
        <main>
          {isLogin ? <Login setUser={setUser} /> : <Signup setUser={setUser} />}
          <p>
            {isLogin ? "¿Aún no tienes cuenta?" : "¿Ya tienes una cuenta?"}
            <button onClick={() => {
              setIsLogin(!isLogin)
            }}>{isLogin ? "Regístrate" : "Ingresa"}</button>
          </p>

        </main>
      }
      {user &&
        <main>
          Esta es tu portada. Bienvenido.
          <LoadData />

        </main>
      }
    </div>
  )
}

export default App
