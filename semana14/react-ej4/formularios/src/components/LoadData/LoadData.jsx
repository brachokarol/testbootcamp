import { useState } from 'react';
import './style.css'

export function LoadData() {
  const [status, setStatus] = useState('idle') // idle, loading, success, error
  const [posts, setPosts] = useState();

  const handleClick = async (event) => {
    setStatus('loading')
    const res = await fetch('https://8gag-api.anxoso.com/posts')
    const data = await res.json()
    console.log('Got:', data)
    if (res.ok) {
      setPosts(data)
      setStatus('success')
    } else {
      setStatus('error')
    }
  }

  if (status === 'loading') {
    return (
      <div>Cargando...</div>
    )
  }

  if (status === 'success') {
    return (
      posts?.map((post) => {
        return (
          <div key={post.id}>
            <h2>{post.title} ({post.author})</h2>
            <img src={post.image} alt={post.title} />
          </div>
        )
      })
    )
  }
  return (
    <button onClick={handleClick}>Cargar Data</button>
  )
}
