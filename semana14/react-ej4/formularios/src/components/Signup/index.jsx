import { useState } from 'react';
import './style.css'

export function Signup({ setUser }) {
  const [status, setStatus] = useState('idle') // idle, loading, success, error
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')

  const handleSubmit = async (event) => {
    event.preventDefault();
    setStatus('loading')
    console.log(username, email, password, repeatPassword);
    const res = await fetch('https://8gag-api.anxoso.com/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password,
      })
    })
    const data = await res.json()
    console.log('Recibido:', data)
    if (res.ok) {
      setUser(data)
      setStatus('success')
    } else {
      setStatus('error')
    }
  }

  if (status === 'loading') {
    return (
      <div>Cargando...</div>
    )
  }

  if (status === 'success') {
    return (
      <div>Bienvenido {username}</div>
    )
  }

  return (
    <form onSubmit={handleSubmit}>
      <label for="nombre">Nombre</label>
      <input value={username} onChange={e => setUsername(e.target.value)} type='text' placeholder='Nombre' id='nombre' name='nombre' />
      <label for="email">Email</label>
      <input value={email} onChange={e => setEmail(e.target.value)} type='email' id='email' name='email' />
      <label for="pwd">Contraseña</label>
      <input value={password} onChange={e => setPassword(e.target.value)} type='password' id='pwd' name='pwd' />
      <label for="rPwd">Repetir contraseña</label>
      <input value={repeatPassword} onChange={e => setRepeatPassword(e.target.value)} type='password' id='rPwd' name='rPwd' />
      <button>Sign up</button>
      {status === 'error' &&
        <p className="error">
          Usuario o contraseña incorrectos.
        </p>
      }
    </form>
  )
}
