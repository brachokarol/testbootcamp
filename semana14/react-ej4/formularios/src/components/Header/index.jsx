import { useState } from 'react';
import './style.css'

export function Header({ user }) {
  return (
    <header>
      <h1>Mi Página</h1>
      {user &&
        <div className="userinfo">
          {user.username}
        </div>
      }
    </header>
  )
}
