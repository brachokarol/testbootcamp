import { useState } from 'react';
import './style.css'

export function Login({ setUser }) {
  const [status, setStatus] = useState('idle') // idle, loading, success, error
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = async (event) => {
    event.preventDefault()
    setStatus('loading')
    const res = await fetch('https://8gag-api.anxoso.com/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password,
      })
    })
    const data = await res.json()
    console.log('Got:', data)
    if (res.ok) {
      setUser(data)
      setStatus('success')
    } else {
      setStatus('error')
    }
  }

  if (status === 'loading') {
    return (
      <div>Cargando...</div>
    )
  }

  if (status === 'success') {
    return (
      <div>Bienvenido {username}</div>
    )
  }

  return (
    <form onSubmit={handleSubmit}>
      <label for="nombre">Nombre</label>
      <input value={username} onChange={e => setUsername(e.target.value)} type='text' placeholder='Nombre' id='nombre' name='nombre' />
      <label for="pwd">Contraseña</label>
      <input value={password} onChange={e => setPassword(e.target.value)} type='password' id='pwd' name='pwd' />
      <button>Login</button>
      {status === 'error' &&
        <p className="error">
          Usuario o contraseña incorrectos.
        </p>
      }
    </form>
  )
}
