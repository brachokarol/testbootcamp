import './Modal.css';

function Modal({ show, children }) {
  return show && (
    <div className='modal-bg'>
      <div className='modal-fg'>
        {children}
      </div>
    </div>
  );
}

export default Modal;