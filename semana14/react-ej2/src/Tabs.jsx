import './Tabs.css'
function Tabs({ current, content }) {
  return (
    <>
      <div className="tabs">
        {content.map((element) => {
          return (
            <span key={element.id} className={`tab ${current === element.id ? 'active' : ''}`}>
              {element.title}
            </span>);
        })}
      </div>
      <div className='contenido'>
        {content.find((element) => element.id === current).content}
      </div>

    </>
  );
}

export default Tabs;