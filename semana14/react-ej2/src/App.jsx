import Spoiler from './Spoiler';
import Semaforo from './Semaforo';
import Tabs from './Tabs';
// import Modal from './Modal';
// import Login from './Login';
import './App.css'
import Counter from './Counter';

function App() {
  const cont = [
    { id: 'readme', title: 'Readme', content: 'Hola que tal' },
    { id: 'code', title: 'Code', content: 'Codigo rojo' },
    { id: 'deps', title: 'Dependency', content: 'Depende... de que depende' },
  ]

  return (
    <>
      <p> Esto se lee, pero...
        <Spoiler> esto sólo al pasar el ratón</Spoiler>
      </p>

      <Semaforo estado="verde" />

      <Tabs current="readme" content={cont} />

      {/* <Modal show>
        <Login />
        <h1>¡Suscríbete a la Newsletter!</h1>
      </Modal > */}

      <Counter />


    </>
  )
}

export default App;
