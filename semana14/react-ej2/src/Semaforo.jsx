import './Semaforo.css';

function Semaforo({ estado }) {
  return (
    <div className='semaforo' >
      <div onClick={() => console.log("Rojo")} className={`rojo ${estado === 'rojo' ? 'active' : ''}`} />
      <div onClick={() => console.log("Amarillo")} className={`amarillo ${estado === 'amarillo' ? 'active' : ''}`} />
      <div onClick={() => console.log("Verde")} className={`verde ${estado === 'verde' ? 'active' : ''}`} />
    </div >

  );
}

export default Semaforo;