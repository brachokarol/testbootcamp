import './Tabs.css'
import { useState } from 'react';

function Tabs({ initial, content }) {
  const [current, setCurrent] = useState(initial);
  return (
    <>
      <div className="tabs">
        {content.map((element) => {
          return (
            <span key={element.id} className={`tab ${current === element.id ? 'active' : ''}`} onClick={() => setCurrent(element.id)}>
              {element.title}
            </span>);
        })}
      </div >
      <div className={`contenido`}>
        {content.find((element) => element.id === current).content}
      </div>

    </>
  );
}

export default Tabs;