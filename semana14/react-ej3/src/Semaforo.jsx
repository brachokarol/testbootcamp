import { useState } from 'react';
import './Semaforo.css';

function Semaforo({ estadoInicial }) {
  const [estado, setEstado] = useState(estadoInicial);
  return (
    <div className='semaforo' >
      <div onClick={() => setEstado("rojo")} className={`rojo ${estado === 'rojo' ? 'active' : ''}`} />
      <div onClick={() => setEstado("amarillo")} className={`amarillo ${estado === 'amarillo' ? 'active' : ''}`} />
      <div onClick={() => setEstado("verde")} className={`verde ${estado === 'verde' ? 'active' : ''}`} />
    </div >
  );
}

export default Semaforo;