import './Acordeon.css';
import { useState } from "react";

function Acordeon({ estadoInicial, children }) {
  const [view, setView] = useState(estadoInicial);
  return (
    <div className="acordeon">
      <button onClick={() => setView("show")}>
        See content!
      </button>
      <div className={`acordeon-body ${view}`}>
        {children}
      </div>
    </div>
  );
}

export default Acordeon;