import './App.css'
import Tienda from './Tienda/App';

function App() {
  const cont = [
    { id: 'readme', title: 'Readme', content: 'Hola que tal' },
    { id: 'code', title: 'Code', content: 'Codigo rojo' },
    { id: 'deps', title: 'Dependency', content: 'Depende... de que depende' },
  ]

  return (
    <>
      <Tienda />
    </>
  )
}

export default App;
