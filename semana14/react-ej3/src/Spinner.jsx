import { useState } from 'react';
import './Spinner.css';

function Spinner() {
  const [n, setN] = useState(0);
  return (
    <>
      <div className='spinner'>
        <span onClick={() => { n > 0 && setN(n - 1) }}>➖</span>
        <span>{n}</span>
        <span onClick={() => { setN(n + 1) }}>➕</span>
      </div>
    </>
  );
}

export default Spinner;