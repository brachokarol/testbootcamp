import './App.css'
import Cart from './Cart'
import Header from './Header'
import Products from './Products'
import { list, add, remove } from '../hooks/useList'

function App() {

  return (
    <div className="app">
      <Header username="Karol" isLoggedIn />
      <div className="body">
        <Products add={add} />
        <Cart />
      </div>
    </div>
  )
}

export default App
