import ProductCard from './ProductCard'
import products from './products.json'
import './Products.css'

function Products({ add }) {
  const [newProduct, setNewProduct] = useState("")

  const handleSubmit = e => {
    e.preventDefault()
    add(newProduct)
    setNewProduct('')
  }

  return (
    <main>
      Productos:
      <div>
        {products.map(product =>
          <div>
            <ProductCard key={product.id} product={product} />
            <button onClick={handleSubmit}>Add</button>
          </div>
        )}
      </div>
    </main>
  )
}

export default Products
