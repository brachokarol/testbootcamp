import PropTypes from 'prop-types'

function CartEntry({ product }) {
  return (
    <div key={product.id} className="cart-entry">
      <img src={product.image} />
      <span>
        {product.title}
      </span>
    </div>
  )
}

CartEntry.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    image: PropTypes.string,
    title: PropTypes.string
  })
}

export default CartEntry
