import { useState } from "react";

function Counter() {
  const [n, setN] = useState(0); //creando un State en React
  function clickCounter() {
    setN(n + 1);
    console.log(`Counter click: ${n}`);
  }

  return (
    <button onClick={clickCounter}>Click Me {n}</button>
  );
}

export default Counter;