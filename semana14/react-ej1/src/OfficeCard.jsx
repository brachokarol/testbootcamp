import './OfficeCard.css';

const office = {
  name: 'Mi Oficina',
  address: 'C/Mayor 123',
  employees: ['Alice', 'Bob'],
  color: "orange",
  isOpen: true,
  category: 'it' //'it, sales, factory'
}

export default function OfficeCard() {
  return (
    <div className='card office-card'>
      <h1 style={{ color: office.color }}>{office.name}</h1>
      <div className={`category ${office.category}`} />
      <address>{office.address}</address>
      <p>Employees qty: {office.employees.length}</p>
      {/* <p style={{ backgroundColor: office.isOpen ? 'darkgreen' : 'darkred' }}> */}
      <h2 className={`office-status ${office.isOpen ? 'open' : 'closed'}`}>
        We are {office.isOpen ? 'open' : 'closed'}
      </h2>
    </div>
  );
}