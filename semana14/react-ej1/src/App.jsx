import OfficeCard from './OfficeCard';
import PokemonCard from './PokemonCard';
import './App.css';

function App() {


  //react fragment <> </> ->se utiliza cuando vamos a renderizar en el return varios contenedores
  //de lo contrario solo se debe enviar un contenedor, por ejemplo todo envuelto en un div, una section, un article, etc. (un solo contenedor)
  return (
    <>
      <OfficeCard />
      <PokemonCard />
    </>
  )
}

export default App
