import './PokemonCard.css';
import ditto from './ditto.json'
import pokeapi from '/pokeapi-logo.png';

export default function PokemonCard() {
  return (
    <div className='card ditto-card'>
      <img src={pokeapi} alt='Pokemon logo' />
      <h1>{ditto.name}</h1>
      <img src={ditto.sprites.front_default} alt='ditto image' />
      <a href={ditto.species.url} target='_blank'>URL</a>
    </div>
  );
}