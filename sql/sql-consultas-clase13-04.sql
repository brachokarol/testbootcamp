/*Listar los nombres de los pacientes y las practicas realizadas de aquellas que superan los 150 en el mes 11.*/
SELECT p.nombre, pr.nombre, pr.precio
FROM pacientes p, practicas pr, consultas c
where pr.precio>150 and month(c.fecha)=11 and
p.id_paciente = c.id_paciente and 
c.id_practica=pr.id_practica;

SELECT p.nombre, pr.nombre, pr.precio
FROM pacientes p
INNER JOIN consultas c ON p.id_paciente = c.id_paciente
INNER JOIN practicas pr ON c.id_practica=pr.id_practica
WHERE pr.precio > 150 AND MONTH(c.fecha)=11;

/*Mostrar el nombre de la obra social, descripción de práctica, precio, y el monto de lo que cubre la mutual para esa práctica,
 de todas aquellas realizadas, ordenadas por mutual y práctica.*/
 select os.nombre as 'Obra social', pr.nombre as Practica, pr.precio, cob.cobertura
 from obrasocial os, practicas pr, cobertura cob, consultas c
 where pr.id_practica=c.id_practica and
 pr.id_practica=cob.id_practica and
 cob.id_obrasocial=os.id_obrasocial group by os.id_obrasocial, pr.id_practica order by os.nombre, pr.nombre;
