-- CONSULTAS ENDPOINTS
-- Listar todas las categorías (getCategories)
SELECT p.*, c.name as category_name
FROM categories c
INNER JOIN place_category pc ON c.id = pc.category_id
INNER JOIN places p ON p.id = pc.place_id
ORDER BY c.id;

-- mostrar y ordenar por votos
SELECT sum(v.vote)/count(v.vote) as votes_average, p.title, p.shortDescription, p.city, p.country
FROM places p
INNER JOIN votes v ON p.id = v.place_id
GROUP BY p.id ORDER BY SUM(v.vote) DESC;


-- listPlaces (muestra todas las entradas, la mas nueva arriba) 
SELECT p.date, p.title, p.shortDescription, p.country, c.name as category, sum(v.vote)/count(v.vote) as votes_average
FROM places p
LEFT JOIN place_category pc ON pc.place_id=p.id
LEFT JOIN categories c ON c.id=pc.category_id
LEFT JOIN votes v ON p.id=v.place_id
GROUP BY p.id
ORDER BY p.date ASC;

-- Listar todo el detalle d eun sitio por id
SELECT p.id as place_id, p.title, p.user_id as posted_by_userID, ph.photo, p.shortDescription, p.largeDescription, p.date as entry_date, p.city, p.country, c.name as main_category, v.vote, v.comment, v.date as comment_date, v.user_id as commented_by_userID
FROM places p 
INNER JOIN place_category pc ON pc.place_id = p.id
INNER JOIN categories c ON pc.category_id = c.id
LEFT JOIN votes v ON p.id=v.place_id
LEFT JOIN photos ph ON p.id = ph.place_id
WHERE p.id=2
GROUP BY p.id;

-- ver detalles de una entrada
SELECT p.id as place_id, p.title, p.user_id as posted_by_userID, p.shortDescription, p.largeDescription, p.date as entry_date, p.city, p.country
FROM places p
WHERE id=1;

SELECT ph.photo
FROM photos ph
WHERE place_id=1;

SELECT v.vote, v.comment, v.date as comment_date, v.user_id as commented_by_userID
FROM votes v
WHERE place_id=1;

 SELECT c.name as tag
 FROM places p
 LEFT JOIN place_category pc ON p.id = pc.place_id
 LEFT JOIN categories c ON pc.category_id = c.id
 WHERE place_id=1;


select email, password from users;
select * from places;
select * from votes;