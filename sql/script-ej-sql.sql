CREATE DATABASE IF NOT EXISTS bootcamp;
USE bootcamp;

drop table students;
drop table addresses;
drop table students_addresses;

-- Ejercicio 1 CREACIÓN DE TABLAS
CREATE TABLE students (
  id_student int NOT NULL auto_increment,
  nombre varchar(45),
  apellido varchar(50),
  email varchar(50),
  telefono varchar(45),
  dni char(11) unique,
  pais varchar(60),
  zip_code varchar (8),
  direccion varchar(60),
  ciudad varchar(60),  
  PRIMARY KEY (id_student)
);

-- Ejercicio 2 borrar direccion de la tabla students y sacarlo a una nueva tabla
-- MODIFICACIÓN DE TABLAS
ALTER TABLE students DROP direccion;
ALTER TABLE students DROP pais;
ALTER TABLE students DROP zip_code;
ALTER TABLE students DROP ciudad;

CREATE TABLE addresses (
  id_address int NOT NULL auto_increment,
  pais varchar(60),
  zip_code varchar (8),
  direccion varchar(60),
  ciudad varchar(60),
  id_student_address int not null,
  PRIMARY KEY (id_address)
);

CREATE TABLE students_addresses (
  id_student_address int not null auto_increment,
  id_student int not null,
  id_address int not null,
  PRIMARY KEY (id_student_address),
  FOREIGN KEY (id_student) REFERENCES students(id_student),
  FOREIGN KEY (id_address) REFERENCES addresses(id_address)
);

-- Ejercicio 3 INSERCIÓN DE DATOS
INSERT INTO students values 
(1,"Irvin","Lethem","ilethem0@google.com.au","993870144","279948941-9"),
(2,"Kylie","Mungan","kmungan1@howstuffworks.com","497494899","748551874-7"),
(3,"Yul","Dibbert","ydibbert2@businesswire.com","776631050","215649413-4"),
(4,"Tamra","Mc Gorley","tmcgorley3@studiopress.com","921948685","617064473-7"),
(5,"Elmira","Imbrey","eimbrey4@cpanel.net","304168000","178988896-4");

INSERT INTO addresses values 
(1,"Indonesia","83297","98339 Loftsgordon Road","Babakanbandung", 1),
(2,"Philippines","44455","74641 Dwight Avenue","Bilar",2),
(3,"Indonesia","62965","9510 Milwaukee Street","Sumberejo",3),
(4,"Norway","54756","8902 Doe Crossing Alley","Steinkjer",4),
(5,"United States","51471","8616 Stephen Hill","Charleston",5);

INSERT INTO students_addresses values 
(1,1,1),
(2,2,2),
(3,3,3),
(4,4,4),
(5,5,5);

SELECT s.*, a.*
FROM students s
inner join students_addresses sa on sa.id_student=s.id_student
inner join addresses a on a.id_address= sa.id_address;

-- Ejercicio 4 SELECCIÓN DE DATOS
/*Selecciona el nombre, apellido y número de teléfono de todos los estudiantes,
ordenados alfabéticamente según su apellido.
Después haz otra consulta que indique cuántos usuarios hay de cada país, 
basándote en la tabla de direcciones.*/

SELECT nombre, apellido, telefono
FROM students
ORDER BY apellido;

SELECT count(s.nombre) as "cantidad usuarios", a.pais
FROM students s
inner join students_addresses sa on sa.id_student=s.id_studentpracticas
inner join addresses a on a.id_address= sa.id_address
GROUP BY a.pais;




