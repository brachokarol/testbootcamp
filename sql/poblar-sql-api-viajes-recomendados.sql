-- Poblando tabla users
INSERT INTO users (email, password) values 
("ilethem0@google.com.au","993870144"),
("kmungan1@howstuffworks.com","497494899"),
("ydibbert2@businesswire.com","776631050"),
("tmcgorley3@studiopress.com","921948685"),
("eimbrey4@cpanel.net","304168000");

-- Poblando tabla places (creando una entrada)
INSERT INTO places (title, shortDescription, city, country, user_id) values 
("Nadando con los tiburones","un día de submarinismo con los tiburones blancos", "Ningaloo", "Australia", 1),
("Avistamiento de ballenas","Ven a ver a las ballenas jorobadas", "Santo Domingo", "Dominican Republic", 1),
("El Salto Ángel","Ven a conocer el salto de agua más alto del mundo", "Canaima", "Venezuela", 2),
("Mercado de San Miguel","Mercado emblemático para los amantes de la buena gastronomía", "Madrid", "Spain", 5);

-- Poblando tabla votes (creando una entrada)
INSERT INTO votes (vote, comment, user_id, place_id) values 
(5,"100% Recommended", 3, 1),
(3,"not so good", 2, 2),
(1,"so bad", 3, 2),
(5,"Amazing", 5, 1),
(5,"Must do", 4, 3),
(5,"Stunning", 1, 4);

-- Poblando tabla categories (creando una entrada)
INSERT INTO categories (name) values 
("Nature"),
("Adventure"),
("Cultural"),
("Sport"),
("Relax"),
("Romantic");

-- Poblando tabla place_category (creando una entrada)
INSERT INTO place_category (place_id, category_id) values 
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 3),
(3, 5),
(3, 6),
(4, 4);

-- CONSULTAS
SELECT p.*, SUM(v.vote) as "Total Votes"
FROM places p
INNER JOIN votes v ON p.id = v.place_id
GROUP BY p.id ORDER BY SUM(v.vote) DESC;

SELECT * 
FROM travelexperience.places as p
INNER JOIN travelexperience.users as u ON p.user_id=u.id;
SELECT * FROM travelexperience.places;




