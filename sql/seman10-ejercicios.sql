-- SEMANA 10 EJERCICIOS
-- EJERCICIO 1
DROP DATABASE IF EXISTS bootcamp;
CREATE DATABASE bootcamp;
USE bootcamp;

CREATE TABLE students (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    lastname VARCHAR(100) NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    tlf INT UNSIGNED,
    dni CHAR(11) UNIQUE NOT NULL
);

CREATE TABLE addresses (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	country VARCHAR(100),
    zip_code CHAR(5),
    address VARCHAR(200),
    city VARCHAR(100),
    id_student INT UNSIGNED NOT NULL,
    FOREIGN KEY (id_student) REFERENCES students (id)
);

INSERT INTO students (name, lastname, email, tlf, dni) VALUES 
('Irvin', 'Lethem', 'ilethem0@google.com.au', 993870144, '279948941-9'),
('Kylie', 'Mungan', 'kmungan1@howstuffworks.com', 497494899, '748551874-7'),
('Yul', 'Dibbert', 'ydibbert2@businesswire.com', 776631050, '215649413-4'),
('Tamra', 'Mc Gorley', 'tmcgorley3@studiopress.com', 921948685, '617064473-7'),
('Elmira', 'Imbrey', 'eimbrey4@cpanel.net', 304168000, '178988896-4');


INSERT INTO addresses (country, zip_code, address, city, id_student) VALUES 
('Indonesia', '83297', '98339 Loftsgordon Road', 'Babakanbandung', 1),
('Philippines', '44455', '74641 Dwight Avenue', 'Bilar', 2),
('Indonesia', '62965', '9510 Milwaukee Street', 'Sumberejo', 3),
('Norway', '54756', '8902 Doe Crossing Alley', 'Steinkjer', 4),
('United States', '51471', '8616 Stephen Hill', 'Charleston', 5);

/*seleccionar todos los datos de los estudiantes incluída toda la información de su dirección.*/
SELECT * 
FROM students s, addresses a
WHERE s.id=a.id_student;

/*selecionar el nombre, apellidos y email de los estudiantes de Indonesia*/
SELECT s.name, s.lastname, s.email
FROM students s
INNER JOIN addresses a ON s.id=a.id_student
WHERE a.country= "Indonesia";

-- EJERCICIO 2
/* Preparación de la base de datos, no edites las siguientes líneas */
DROP DATABASE IF EXISTS bootcamp;
CREATE DATABASE bootcamp;
USE bootcamp;

CREATE TABLE students (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    lastname VARCHAR(100) NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    tlf INT UNSIGNED,
    dni CHAR(11) UNIQUE NOT NULL
);

INSERT INTO students (name, lastname, email, tlf, dni) VALUES 
('Irvin', 'Lethem', 'ilethem0@google.com.au', 993870144, '279948941-9'),
('Kylie', 'Mungan', 'kmungan1@howstuffworks.com', 497494899, '748551874-7'),
('Yul', 'Dibbert', 'ydibbert2@businesswire.com', 776631050, '215649413-4'),
('Tamra', 'Mc Gorley', 'tmcgorley3@studiopress.com', 921948685, '617064473-7'),
('Elmira', 'Imbrey', 'eimbrey4@cpanel.net', 304168000, '178988896-4');

/*Modifica la tabla students (usando ALTER TABLE) para añadir una columna para su edad (age)*/
ALTER TABLE students ADD age int;

/*A continuación rellena esa columna para los 5 estudiantes que hay en la tabla con los valores que tu decidas.*/
UPDATE students SET age=19 where students.id=1;
UPDATE students SET age=33 where students.id=2;
UPDATE students SET age=24 where students.id=3;
UPDATE students SET age=28 where students.id=4;
UPDATE students SET age=21 where students.id=5;

-- EJERCICIO 3
/*seleccionar el nombre y la edad de el/los usuarios con mayor edad.
Para ello tienes que usar subconsultas (…y una pista: la función max de SQL).*/
SELECT name, age
FROM students
WHERE age=(SELECT max(age) FROM students);