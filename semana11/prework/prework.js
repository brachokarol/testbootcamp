// node prework.js --inputDir=images --outputDir=result --watermark=kb.png --resize=500
const minimist = require('minimist');
const chalk = require('chalk');
const path = require('path');
const fs = require('fs/promises');
const sharp = require('sharp');
const { pathExists, createPathIfNotExists } = require('./helpers');
//Proceso los argumentos
const args = minimist(process.argv.slice(2));
const { inputDir, outputDir, watermark, resize } = args;

//Mensaje de bienvenida al programa
console.log(chalk.blue("Welcome to Image Process"));

//Esta función hace el trabajo de procesado de imágenes
async function processImages({ inputDir, outputDir, watermark, resize }) {
  const inputPath = path.resolve(__dirname, inputDir);
  const outputPath = path.resolve(__dirname, outputDir);
  let watermarkPath;
  try {
    //si existe el argumento watermark, asignar su ruta
    if (watermark) {
      watermarkPath = path.resolve(__dirname, watermark);
    }

    //comprobar que inputDir existe
    await pathExists(inputPath);

    //crear si no existe outputDir
    await createPathIfNotExists(outputPath);

    //comprobar si la ruta a watermark existe
    await pathExists(watermarkPath);

    //Leer los archivos de inputPath
    const inputFiles = await fs.readdir(inputPath);

    //Quedarme solo con los archivos que sean imágenes
    const imageFiles = inputFiles.filter((file) => {
      const validExtensions = ['.jpg', '.jpeg', '.gif', '.png', '.webp', 'avif', 'tiff', 'svg'];
      return validExtensions.includes(path.extname(file).toLowerCase());
    });

    //Recorrer la lista de archivos y:
    // - Si existe "resize" redimensionar cada una de las imágenes
    // - Si existe "watermark" colocar el watermark en la parte superior izquierda de la imagen
    // - Guardar la imagen resultante en outputPath
    for (const imgFile of imageFiles) {
      console.log(chalk.blue(`Procesando la imagen: ${imgFile}`));
      //Creamos la ruta completa de la imagen
      const imagePath = path.resolve(inputPath, imgFile);

      //Cargamos la imagen en sharp
      const image = sharp(imagePath);
      //resize
      if (resize) {
        image.resize(resize);
      }
      //watermark
      if (watermark) {
        image.composite([{
          input: watermarkPath,
          top: 10,
          left: 10,
        }]);
      }

      //Guardamos la imagen con otro nombre en outputPath
      await image.toFile(path.resolve(outputPath, `processed_${imgFile}`));
    }

  } catch (error) {
    console.error((chalk.red(error.message)));
    console.error(chalk.red("Comprueba que los argumentos sean correctos"));
    process.exit(1);
  }
}

//Si no existe inputDir o outputDir muestro un error y salgo del programa
if (!inputDir || !outputDir) {
  console.log(chalk.red("Los argumentos --inputDir y --outputDir son obligatorios"));
  process.exit(1);
}

//Si no existe watermark y resize salgo del programa (debe existir por lo menos uno)
if (!watermark && !resize) {
  console.log(chalk.red("Es necesario que exista un argumento --watermark o --resize"));
  process.exit(1);
}

console.log(chalk.green("Procesando imágenes..."));

processImages({ inputDir, outputDir, watermark, resize });