const express = require("express");
const app = express();

app.use((req, res, next) => {
  console.log("Url ->", req.url);
  console.log("Method ->", req.method);
  // console.log("User agent ->", req.header("User-Agent"));
  next();
});

app.get("/error-forzado", (req, res, next) => {
  return next(new Error("Este es un error generado intencionadamente"));
});

app.get("/time", (req, res, next) => {
  try {
    const now = new Date();
    const hour = now.getHours();
    const minutes = now.getMinutes();
    // Si tengo un error voy al middleware de los errores
    // throw new Error("Error generado");
    res.send({
      status: "ok",
      msg: `Request time: ${hour}:${minutes}`,
    });
  } catch (e) {
    next(e);
  }
});

app.get("/path", (req, res, next) => {
  try {
    res.send({
      status: "ok",
      msg: "The server is at: " + __dirname,
    });
  } catch (e) {
    next(e);
  }
});

app.use((err, req, res, next) => {
  console.log(err.message);
  res.status(500).send({
    status: "error",
    msg: "Something bad happend",
  });
});

app.use((req, res) => {
  res.status(404);
  res.send({
    status: "error",
    msg: "Page not found",
  });
});

app.listen(3000, () => console.log("Listening on `http://127.0.0.1:3000`..."));
