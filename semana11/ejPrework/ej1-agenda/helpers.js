const fs = require('fs/promises');
const chalk = require('chalk');
const { log } = require('console');
//comprueba si el path existe
async function pathExists(path) {
  try {
    await fs.access(path);
    console.log(chalk.yellow("Guardando datos en agenda..."));
    return true;
  } catch {
    console.log(chalk.yellow("Creando nueva agenda..."));
  }
}

async function createPathIfNotExists(path, data) {
  try {
    await fs.writeFile(path, data);
    console.log(chalk.green("Agenda creada exitosamente en: ") + chalk.yellow(path));
  } catch (error) {
    console.error("Error al guardar en agenda");
  }
}

async function listEvents(path) {
  try {
    await fs.access(path);
    const data = await fs.readFile(path, 'utf-8');
    const parseData = JSON.parse(data);
    parseData.forEach(element => {
      console.log(`📆 ${Object.values(element).join(": ")}`);
    });
  } catch {
    console.error(chalk.red("No hay eventos guardados"));
  }
}

module.exports = {
  pathExists,
  createPathIfNotExists,
  listEvents
}