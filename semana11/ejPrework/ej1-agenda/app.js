const args = require('minimist')(process.argv.slice(2));
const fs = require('fs/promises');
const chalk = require('chalk'); //muestra el resultado del console.log en colores
const path = require('path');
const agendaPath = path.resolve(__dirname, "agenda.json");
const { pathExists, createPathIfNotExists, listEvents } = require('./helpers');
const moment = require('moment'); //validador de fechas


async function saveAgenda({ date, event }) {
  try {
    //comprobar si la agenda existe:
    // - Si existe, no sobreescribir el archivo
    // - Si no existe, crearla
    const isExists = await pathExists(agendaPath);
    if (isExists) {
      //leo el archivo
      const agenda = await fs.readFile(agendaPath);
      //Hago un parse de los datos que trae el archivo para poder hacerle push de los nuevos datos
      const parseAgenda = JSON.parse(agenda);
      parseAgenda.push({ date, event });
      //Guardo el nuevo objeto en formato json
      const dataTostring = JSON.stringify(parseAgenda);
      await fs.writeFile(agendaPath, dataTostring);
      console.log(chalk.bgBlue("Evento guardado correctamente"));

    } else {
      createPathIfNotExists(agendaPath, JSON.stringify([{ date, event }]));
    }

  } catch (error) {
    console.error(chalk.red(error.message));
  }
}

try {
  //proceso los argumentos
  const { date, event } = args;
  //Verifico si la fecha es válida (modo estricto con el true al final) y la guardo en isDate
  const isDate = moment(date, "YYYY/MM/DD", true).isValid();
  //verifico si solicitan listar los eventos
  const showEvents = Object.keys(args).includes('list-events');
  if (showEvents) {
    listEvents(agendaPath);
  }
  //verifico que exista la fecha y que su formato sea válido, verifico el evento 
  else if (!isDate || !event) {
    throw new Error("Los argumentos no son válidos");
  } else {
    saveAgenda({ date, event });
  }

} catch (error) {
  console.error(chalk.red(error.message));
}