const arg = process.argv[2];
const path = require('path');
const sharp = require('sharp');
const { pathExists, isImage, createPathIfNotExists } = require('./helpers');

//comprobaciones si la ruta del archivo existe
try {
  const imagePath = path.join(__dirname, arg);
  pathExists(imagePath);

  imageProcessor(imagePath);

} catch (error) {
  console.error(error.message);
  process.exit(1);
}

//función que procesa la imagen
async function imageProcessor(filePath) {
  //verificar si el archivo es un formato válido de imagen
  isImage(filePath);
  //crear (si no existe) directorio donde guardar las imagenes processadas
  outputPath = path.join(__dirname, 'sample');
  createPathIfNotExists(outputPath);

  /** Procesar imágenes
  * miniatura de 200px de ancho
  * tamaño medio de 500px de ancho
  * tamaño medio del tamaño anterior pero en blanco y negro */

  const image = sharp(filePath); //cargar imagen al sharp
  //cortar imagen miniatura= 200px
  image.resize(200);
  await image.toFile(path.join(outputPath, `rsz_small_${path.basename(filePath)}`));
  //imagen mediana
  image.resize(500);
  await image.toFile(path.join(outputPath, `rsz_medium_${path.basename(filePath)}`));
  //escala de grises tamaño medio del mediano
  image.resize(500 / 2).grayscale().toFile((path.join(outputPath, `rsz_grayScale_${path.basename(filePath)}`)));

}
