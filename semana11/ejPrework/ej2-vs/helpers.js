const fs = require('fs/promises');
const chalk = require('chalk');
const pathModule = require('path');

//Función que comprueba que una ruta existe en disco
async function pathExists(path) {
  try {
    await fs.access(path);
  } catch {
    console.error(chalk.red(`La ruta NO EXISTE ${path}`));
    process.exit(1);
  }
}

function isImage(path) {
  try {
    const validExtensions = ['.jpg', '.jpeg', '.gif', '.png', '.webp', 'avif', 'tiff', 'svg'];
    const isImg = validExtensions.includes(pathModule.extname(path).toLowerCase());
    if (!isImg) {
      throw new Error()
    }
  } catch (error) {
    console.error(chalk.red(`El archivo no es una imagen`));
    process.exit(1);
  }
}

async function createPathIfNotExists(path) {
  try {
    await fs.access(path);
  } catch {
    await fs.mkdir(path);
  }
}


module.exports = {
  pathExists,
  isImage,
  createPathIfNotExists
}
