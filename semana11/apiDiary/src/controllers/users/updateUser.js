const getDB = require('../../database/db');

async function updateUser(req, res) {
  try {
    //tomo el id que viene en el parametro de la request
    const { id } = req.params;
    const { name, pwd } = req.body;
    //conectar con la base de datos
    const connect = await getDB();

    //actualizar(modificar) datos con patch
    const [users] = await connect.query(
      `UPDATE users SET name=?, password=? WHERE id=?`, [name, pwd, id]
    )
    return res.status(200).send(
      {
        "status": "ok",
        "mensaje": "Usuario actualizado correctamente"
      });

  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
}

module.exports = updateUser;