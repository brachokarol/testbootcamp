const getDB = require('../../database/db');

async function getUser(req, res) {
  try {
    //tomo el id que viene en el parametro de la request
    const { id } = req.params;
    //conectar con la base de datos
    const connect = await getDB();
    //guardo en un array la info que me devuelve la query pasada en el parametro de connect.query
    const [users] = await connect.query(
      `SELECT * FROM users WHERE id=?`, [id]
    )
    //si existe el resultado, enviarlo al front
    if (users.length) {
      return res.status(200).send(
        {
          "status": "ok",
          "data": users
        });

    } else {
      res.status(400).send("Usuario no encontrado");
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = getUser;