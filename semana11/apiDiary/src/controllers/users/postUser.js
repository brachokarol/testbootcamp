const getDB = require('../../database/db');

async function postUser(req, res) {
  try {
    //tomo el id que viene en el parametro de la request
    const { email } = req.body;
    //conectar con la base de datos
    const connect = await getDB();

    //consulto primero si los datos existen para después guardarlos
    const [userExists] = await connect.query(
      `SELECT id FROM users WHERE email=?`[email]
    );
    if (userExists.length > 0) {
      return res.send(
        {
          "status": "bad",
          "mensaje": "El usuario ya existe"
        }
      )
    }
    //insertar datos
    const [users] = await connect.query(
      `INSERT INTO users (email, name, password) values (?,?,?)`, [email, name, pwd]
    )
    return res.status(200).send(
      {
        "status": "ok",
        "data": users
      });

  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
}

module.exports = postUser;