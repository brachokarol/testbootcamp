//Conexión a la base de datos
//instalar driver: npm install mysql2
const mysql = require('mysql2/promise');

//subo al process.env mis variables de entorno contenidas en el archivo .env
require('dotenv').config()

//requerir las variables de entorno guardadas en el proceso
//las mismas están en mi archivo .env que cree con el npm dotenv
const { HOST, USER, PASSWORD, DATABASE } = process.env;

/** esta función crea un pool de conexión a la DB (si no existe).
 y devuelve una conexión
*/
let pool;
async function getDB() {
  if (!pool) {
    pool = mysql.createPool({
      connectionLimit: 10,
      host: HOST,
      user: USER,
      password: PASSWORD,
      database: DATABASE
    });
  }
  return await pool.getConnection();
}

module.exports = getDB;