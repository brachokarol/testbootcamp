//aquí escribo los endpoints de user
const express = require('express');
const router = express.Router(); //instanciamos un enrutador

const getUser = require('../controllers/users/getUser');
const postUser = require('../controllers/users/postUser');
const updateUser = require('../controllers/users/updateUser');

router.get('/users/:id', getUser);
router.post('/users', postUser);
router.patch('/users/:id', updateUser);

module.exports = router;