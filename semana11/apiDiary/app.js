const express = require('express'); //npm install express
const morgan = require('morgan'); //middleware

const userRouter = require('./src/routes/userRouter'); //importo el router

const app = express(); //instanciamos un servidor llamado app

//middlewares globales
app.use(morgan('dev'));
app.use(express.json()); //parsea el body

//Al usar la siguiente instrucción, llamo los endpoints que escribí en el archivo userRouter
app.use(userRouter);



//configuración del puerto que escucha la petición
app.listen(3000, () => { console.log("Servidor escuchando en el puerto 3000"); });