import './App.css';
function App() {
  const colors = {
    r: Math.floor(Math.random() * 256),
    g: Math.floor(Math.random() * 256),
    b: Math.floor(Math.random() * 256)
  }
  return (
    <div className="App">
      <h1 style={{ backgroundColor: `rgb(${colors.r},${colors.g},${colors.b})` }}>Hola desde React</h1>
      <p className="importante">Este es un párrafo cualquiera </p>
    </div >
  );
}

export default App;
