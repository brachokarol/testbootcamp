import './App.css';
import Comments from './Comments';
import Actions from './Actions';
import Statistics from './Statistics';
import PostComment from './PostComment';
import Header from './Header';
import PostImage from './PostImage';
import PostDescription from './PostDescription';


function App() {
  return (
    <main className="app">
      <PostImage />
      <aside>
        <Header />
        <PostDescription />
        <Comments />
        <Actions />
        <Statistics />
        <PostComment />
      </aside>
    </main>
  );
}

export default App;
