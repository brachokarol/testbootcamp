import CommentCard from "./CommentCard"
import CommentList from "./Comments.json"
import "./Comments.css"

function Comments() {
  return (
    <article className="comments-container" >
      <section className="comments">
        {CommentList.map(comment =>
          comment.postId === 1 &&
          <CommentCard key={comment.id} comment={comment} />
        )}
      </section>
    </article>
  )
}

export default Comments