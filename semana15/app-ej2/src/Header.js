import User from "./User";
import './Header.css'

export default function Header() {
  return (
    <>
      <div className="header">
        <User />
        <span>Follow</span>
        <span>...</span>
      </div>
    </>
  )
}