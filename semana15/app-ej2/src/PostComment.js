import './PostComment.css'
function PostComment() {
  return (
    <div className="post-comment">
      <span>☺</span>
      <input type="text" placeholder="Add a comment..." />
      <span>Post</span>
    </div>
  )

}

export default PostComment