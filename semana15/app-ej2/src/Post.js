import PostImage from "./PostImage"
import PostDescription from "./PostDescription"
import User from "./User"

function Post() {
  return (
    <>
      <section className="description">
        <PostImage />
        <User />
        <PostDescription />
      </section>
    </>
  )

}

export default Post