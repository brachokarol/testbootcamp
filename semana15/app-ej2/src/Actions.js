import "./Actions.css"
function Actions() {
  return (
    <>
      <div className="actions">
        <div>
          <span>🤍</span>
          <span>💬</span>
          <span>↗</span>
        </div>
        <span className="guardar-post">💌</span>
      </div>
    </>
  )

}

export default Actions