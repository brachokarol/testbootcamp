import "./CommentCard.css"
function CommentCard({ comment }) {
  return (
    <>
      <div className='comment-card'>
        <span className="postName">{comment.email} </span>
        <span className="postDescription">{comment.body}</span>
      </div>
    </>
  )
}

export default CommentCard