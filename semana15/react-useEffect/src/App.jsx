import './App.css'
import { useState } from 'react'
import Acordeon from './Acordeon'
import Counter from './Counter'
import Countdown from './Countdown'
import PokemonCard from './PokemonCard'
import Products from './Products'
import Resolution from './Resolution'
import Landscape from './Landscape'
import Video from './Video'
import Audio from './Audio'

function App() {
  const [count, setCount] = useState(0)


  console.log('Render')

  return (
    <>
      <button onClick={() => setCount((count) => count + 1)}>
        count is {count}
      </button>
      <Acordeon title="Countdown">
        <Countdown />
      </Acordeon>
      <Acordeon title="Counter">
        <Counter />
      </Acordeon>
      <Acordeon title="Show Pokemon">
        <PokemonCard id="28" />
      </Acordeon>
      <Acordeon title="Tienda">
        <Products />
      </Acordeon>
      <Acordeon title="Resolution">
        <Resolution />
        <Landscape />
      </Acordeon>
      <Acordeon title="Video">
        <Video />
      </Acordeon>
      <Acordeon title="Audio">
        <Audio />
      </Acordeon>
    </>
  )
}

export default App
