import { useEffect, useState } from "react";
import './Products.css'

export default function Products() {
  const [products, setProducts] = useState()
  useEffect(() => {
    const loadData = async () => {
      const res = await fetch(`https://fakestoreapi.com/products`)
      const data = await res.json()
      setProducts(data)
    }
    loadData()
  }, [])
  return products && (
    <div>
      {products?.map(product =>
        <div className="product-card" key={product.id}>
          <img src={product.image} />
          <span>
            {product.title}
          </span>
        </div>
      )}
    </div>
  )
}