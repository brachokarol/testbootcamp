import { useEffect, useState } from 'react'

function Countdown() {
  const [timerEnded, setTimerEnded] = useState(false)

  useEffect(() => {
    console.log('Effect')
    const t = setTimeout(() => {
      console.log('Timer ended')
      setTimerEnded(true)
    }, 5000)
    return () => {
      console.log('Effect is gone')
      clearTimeout(t)
    }
  }, [])

  return timerEnded ? <h1>Se acabó el tiempo!</h1> : <div>Espera...</div>

}

export default Countdown
