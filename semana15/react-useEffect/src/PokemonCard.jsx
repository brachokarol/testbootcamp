import { useEffect, useState } from "react";
import './PokemonCard.css'

export default function PokemonCard({ id }) {
  const [info, setinfo] = useState()
  useEffect(() => {
    const ShowPokemon = async () => {
      const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
      const data = await res.json()
      setinfo(data)
    }
    ShowPokemon()
  }, [id])
  return info && (
    <div className="poke-card">
      <h1>{info.name}</h1>
      <img src={info.sprites.front_default} alt={info.name} />
      <a href={info.species.url} target='_blank'>URL</a>
    </div>
  );
}