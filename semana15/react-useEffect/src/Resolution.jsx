import { useEffect, useState } from "react"

function Resolution() {
  const [size, setSize] = useState(`${window.innerWidth}x${window.innerHeight}`)

  useEffect(() => {
    const handler = () => setSize(`${window.innerWidth}x${window.innerHeight}`)
    window.addEventListener('resize', handler)
    return () => window.removeEventListener('resize', handler)
  }, [])

  return (
    <div>{size}</div>
  )
}

export default Resolution
