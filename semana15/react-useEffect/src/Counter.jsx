import { useEffect, useState } from 'react'

function Countdown() {
  const [n, setN] = useState(0)

  useEffect(() => {
    console.log('Effect')
    const t = setInterval(() => {
      setN(n => n + 1)
    }, 1000)
    return () => {
      clearInterval(t)
    }
  }, [])

  return n

}

export default Countdown
