function App() {
  return (
    <div className="App">
      <h1>{process.env.REACT_APP_TITLE}</h1>
      <img
        src={process.env.PUBLIC_URL + '/foto.jpg'}
        alt={process.env.REACT_APP_TITLE}
        style={{ width: '300px' }}
      />
    </div>
  );
}

export default App;
