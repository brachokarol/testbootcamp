"use strict"
/* Partiendo del array de puntuaciones propuesto 
imprimir por la consola el mejor y peor equipo 
con su total de puntos respectivo.

Resultado:
El mejor equipo es Toros Negros con un total de 28 puntos
El peor equipo es Ciervos Celestes con un total de 10 puntos
*/

// puntuaciones primera ronda
const firstRound = [
  { team: "Toros Negros", scores: [1, 3, 4, 2, 10, 8] },
  { team: "Águilas Plateadas", scores: [5, 8, 3, 2, 5, 3] },
  { team: "Leones Carmesí", scores: [5, 4, 3, 1, 2, 3, 4] },
  { team: "Rosas Azules", scores: [2, 1, 3, 1, 4, 3, 4] },
  { team: "Mantis Verdes", scores: [1, 4, 5, 1, 3] },
  { team: "Ciervos Celestes", scores: [3, 5, 1, 1] },
  { team: "Pavos Reales Coral", scores: [2, 3, 2, 1, 4, 3] },
  { team: "Orcas Moradas", scores: [2, 3, 3, 4] },
];

const teamTotalScore =
  firstRound.map((elemento) => {
    return {
      team: elemento.team,
      totalScore: elemento.scores.reduce((acc, elemento) => {
        return acc + elemento;
      })
    }

  })
    .sort((a, b) => {
      return b.totalScore - a.totalScore //b-a ordena de mayor a menor
    });

const menorPuntaje = teamTotalScore[teamTotalScore.length - 1];
const mayorPuntaje = teamTotalScore[0];

console.log(
  `El mejor equipo es ${mayorPuntaje.team} con un total de ${mayorPuntaje.totalScore} puntos`
);
console.log(
  `El peor equipo es ${menorPuntaje.team} con un total de ${menorPuntaje.totalScore} puntos`
);