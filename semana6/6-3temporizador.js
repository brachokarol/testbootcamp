/*
Programa que imprime en la consola cada 5 segundos el tiempo que ha pasado 
desde la ejecución del mismo.
Ejemplo output:
...
Han pasado 0 días, 0 horas, 1 minuto y 20 segundos desde la ejecución del programa
Han pasado 0 días, 0 horas, 1 minuto y 25 segundos desde la ejecución del programa
...

Crea una función que se encargue de parar el temporizador anterior después de un tiempo arbitrario.
Esta función recibirá 2 parámetros: un número y una letra: D, H, M o S 
(correspondiente a Días, Horas, Minutos o Segundos). Estos parámetros determinarán el tiempo que 
tardará la función en parar el temporizador inicial 
(ej. los parámetros 2, "M" harán que el temporizador se pare a los 2 minutos).
Cuando pare el temporizador debe mostrar un mensaje en la consola indicando que se paró.
*/
//Initializing counters
let secs = 1; //segundos
let mins = 0; //minutos
let hrs = 0; //horas
let days = 0; //días
let sLetter;

//SETTING TIMERS
//temporizador de segundos
const s = setInterval(() => {
  secs++
}, 1000);
//temporizador de minutos
const m = setInterval(() => {
  mins++
}, (60 * 1000));
//temporizador de horas
const h = setInterval(() => {
  hrs++
}, (3600 * 1000));
//temporizador de días
const d = setInterval(() => {
  days++
}, (86400 * 1000));

//Output, callback to print the timer
const timerPrint = () => {
  console.log(`Han pasado ${days} día${days === 1 ? sLetter = "" : sLetter = "s"}, ${hrs} hora${hrs === 1 ? sLetter = "" : sLetter = "s"}, ${mins} minuto${mins === 1 ? sLetter = "" : sLetter = "s"} y ${secs} segundos desde la ejecución del programa`);
}

//Initial timer, this timer call the timerPrint each 5 seconds
const initialTimer = setInterval(timerPrint, 5000);


//stop timer function receiving two arguments
const stopTimer = (num, letter) => {
  //Following switch evaluates the letter received as argument to converts the num into the corresponding milliseconds
  switch (letter.toLowerCase()) {
    case 's':
      num = num * 1000;
      break;
    case 'm':
      num = num * 60000;
      break;
    case 'h':
      num = num * 3600000;
      break;
    case 'd':
      num = num * 86400000;
      break;
  }
  //stop the setIntervals
  setTimeout(() => {
    clearInterval(initialTimer);
    clearInterval(s);
    clearInterval(m);
    clearInterval(h);
    clearInterval(d);
    console.log("El programa ha finalizado.");
  }, num);
}

//call to stop timer with arbitrary parameters
stopTimer(1, "m");