"use strict";

// Implementar un script que visualiza un contador.
// Cada segundo cambiar el valor del contador y poner un color de fondo random

let contador = 1;
const pElement = document.querySelector("p");
const bodyElement = document.querySelector("body");

setInterval(() => {
  pElement.textContent = contador++;
  let rndRojo = Math.floor(Math.random() * 256);
  let rndVerde = Math.floor(Math.random() * 256);
  let rndAzul = Math.floor(Math.random() * 256);
  bodyElement.style.cssText = `
  background-color: rgb(${rndRojo}, ${rndVerde}, ${rndAzul});
  `;

}, 1000);
