"use strict"

function validateDNI(dni) {
  //letras del DNI
  const letras = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
  //Separamos el dni en num y letra

  const dniPartes = dni.split("-");
  const dniNum = dniPartes[0];
  const dniLetra = dniPartes[1].toUpperCase();
  //Calculo el numero de posicion de la letra
  const posLetra = dniNum % 23;

  try {
    //validamos el dni y la letra
    if (dniNum.length === 8 &&  //dniNum tiene 8 digitos?
      dniNum != "00000000" &&   //dni mayor que cero?
      dniLetra.length === 1 && //dniLetra tiene 1 caracter?
      isNaN(dniLetra) &&      //dniLetra no es un número?
      letras[posLetra] === dniLetra) {  //la letra corresponde al algoritmo?
      return true;
    }
    let errorMsg = "DNI no valido";
    if (dniNum.length !== 8) {
      errorMsg = `El DNI debe tener 8 dígitos`;
    } else if (dniNum === "00000000") {
      errorMsg = `El DNI no existe`;
    } else if (dniLetra.length != 1 || !isNaN(dniLetra)) {
      errorMsg = `El DNI sólo puede contener una letra`;
    } else if (letras[posLetra] !== dniLetra) {
      errorMsg = `La letra no corresponde al número de DNI`;
    }
    //creo un error y le asigno el mensaje almacenado en result
    throw new Error(errorMsg);
  } catch (error) {
    console.error("Se ha producido un error:", error.message);
  }
}


if (validateDNI('00000001-T')) {
  console.log("DNI VALIDO")
}

// dni validos: 26590523-q , 46336871-j, 00000001-R
// dni válido en formato pero no existente 00000000-b
// Nota: probaremos con varios números de DNI tanto válidos como no válidos
