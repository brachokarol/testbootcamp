import { Menu } from "./Menu"
import { SearchBox } from "./SearchBox"
import { TitlePage } from "./TitlePage"
import "./Header.css"

export const Header = () => {
  return (
    <header className="header">
      <img className="banner"
        src={process.env.PUBLIC_URL + '/banner-img.jpg'}
        alt="Globo aerostático" />
      <Menu />
      <TitlePage />
      <SearchBox />
    </header>
  )
}