import "./SearchInput.css"
export default function SearchInput({ searchTerm, setSearchTerm }) {
  return (
    <form name="search-form" id="search-form">
      <input
        type="search"
        value={searchTerm}
        onChange={(e) => { setSearchTerm(e.target.value) }}
        placeholder="Buscar lugares/experiencias"
      />
      <img
        src={process.env.PUBLIC_URL + '/icon-search.png'}
        alt={`Buscar`}
      />
    </form>
  )
}