import "./SearchBox.css"
import { useState } from "react";
import { SearchItems } from "./SearchItems";
import SearchInput from "./SearchInput";
export const SearchBox = () => {
  const [searchTerm, setSearchTerm] = useState("");
  return (
    <>
      <SearchItems />
      <SearchInput searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
    </>
  )
}