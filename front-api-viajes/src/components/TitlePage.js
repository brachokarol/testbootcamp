import { Eslogan } from "./Eslogan"
import "./TitlePage.css"
export const TitlePage = () => {
  return (
    <section className="title-page">
      <h1>Travel Experiences</h1>
      <Eslogan />
    </section>
  )
}