import "./SearchCard.css"
export const SearchCard = ({ title = "lugares", fileName }) => {
  return (
    <span className="search-card">
      <h3>{title}</h3>
      <img
        src={process.env.PUBLIC_URL + '/' + fileName}
        alt={`Búsqueda por ${title}`}
      />
    </span>
  )
}