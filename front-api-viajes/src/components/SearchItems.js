import "./SearchItems.css"
import { SearchCard } from "./SearchCard"
export const SearchItems = () => {
  return (
    <section className="search-items">
      <SearchCard title="Categorías" fileName="icon-categories.png" />
      <SearchCard title="Países" fileName="icon-countries.png" />
      <SearchCard title="Ciudades" fileName="icon-cities.png" />
    </section>
  )
}