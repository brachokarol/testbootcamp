'use-strict';
const express = require('express');
const fs = require('fs/promises');
const path = require('path');
const Joi = require('joi');
const port = 3000;

const app = express();
app.use(express.json());

const dataPath = path.join(__dirname, 'data.json');

app.get('/list', async (req, res) => {
  try {
    const data = await readFile();
    if (data.length > 0) {
      return res.status(200).send(
        {
          "status": "ok",
          "data": data
        });
    } else {
      throw new Error("There is not data to display");
    }
  } catch (error) {
    return res.status(404).send(
      {
        "status": "Cannot get the data",
        "data": error.message
      });
  }
});

app.post('/new', async (req, res) => {
  try {
    const data = await readFile();
    const { date, event } = req.body;
    const dateSchema = Joi.date();
    const validationDate = dateSchema.validate(date);
    const eventSchema = Joi.string();
    const validationEvent = eventSchema.validate(event);
    if (validationDate.error) {
      throw new Error("Format date not valid, try 'YYYY-MM-DD'");
    } else if ((validationEvent.error)) {
      throw new Error("Event must be a string");
    }
    data.push({ date, event });
    const dataTostring = JSON.stringify(data)
    await fs.writeFile(dataPath, dataTostring);
    res.status(200).send({
      "status": "ok",
      "message": "Event saved succesfully"
    });

  } catch (error) {
    return res.status(500).send(
      {
        "status": "Cannot save the data",
        "data": error.message
      });
  }
});

async function readFile(req, res) {
  try {
    await fs.access(dataPath);
    const data = await fs.readFile(dataPath);
    const parseData = JSON.parse(data);
    return parseData;
  } catch (error) {
    error.message = "The path doesn't exists";
    throw error;
  }
}

app.listen(port, () => { console.log(`Server listening on port ${port}`); });