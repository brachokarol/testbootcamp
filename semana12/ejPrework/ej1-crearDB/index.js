'use strict';
const getDB = require('./db');

async function createDB(req, res) {
  try {
    //conexión con la db
    const connect = await getDB();
    await connect.query(`CREATE DATABASE IF NOT EXISTS web;`);
    await connect.query(`USE web`);
    await connect.query(`drop table likes`);
    await connect.query(`drop table photos`);
    await connect.query(`drop table users`);
    await connect.query(`CREATE TABLE users (id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT, email TEXT(100) NOT NULL, name TEXT(100), registration_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, active BOOLEAN DEFAULT false);`);
    await connect.query(`CREATE TABLE photos (id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT, user_id INT UNSIGNED NOT NULL, photo_file_name TEXT(100) NOT NULL, creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (id) REFERENCES users(id));`);
    await connect.query(`CREATE TABLE likes (id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT, user_id INT UNSIGNED NOT NULL, photo_id INT UNSIGNED NOT NULL, FOREIGN KEY (user_id) REFERENCES users(id), FOREIGN KEY (photo_id) REFERENCES photos(id), creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);`);
    connect.release();
    console.log("Database and tables created succesfully");
  } catch (error) {
    console.log(error);
  }
}

createDB();

